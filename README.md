# ISP Demo Project
This is a demo application to showcase how a custom software solution can support business processes.
Build with java, spring boot and vaadin. 
Hosted with Docker, Kubernetes (k3s) and flux.


## The Idea
The goal was to build a Software Solution to support the contract management of an Internet Service Provider. The contract has several differnet stages, in which the contract has to meet certain criteria.


## Structure:
/app -> the sourcecode
/design -> the design mockups build with figma


