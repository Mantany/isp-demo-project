package de.fhwedel.pimpl.view.contract;


import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.service.ContractService;
import de.fhwedel.pimpl.view.MainView;
import de.fhwedel.pimpl.view.contract.component.ContractList;

/**
 * The ContractList view to show contracts in a list.
 * @author Lennard Trippensee(imca103936)
 *
 */
@Route(value = "contracts", layout = MainView.class)
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class ContractListView extends VerticalLayout implements BeforeEnterObserver {
	private ContractService contr_service;
	private ContractList cl;
	private H1 title;
	
	public ContractListView(ContractService contr_service) {
		this.contr_service = contr_service;
		this.cl = new ContractList(this.contr_service);
		this.title = new H1("Verträge - Überblick");
		add(title);
		add(cl);
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		this.cl.updateList();
		
	}
	
}
