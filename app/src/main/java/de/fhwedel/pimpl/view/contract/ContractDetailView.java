package de.fhwedel.pimpl.view.contract;

import java.util.Optional;
import java.util.function.Consumer;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.dom.Style;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Contract.ContractStatus;
import de.fhwedel.pimpl.data.service.ContractService;
import de.fhwedel.pimpl.data.service.CustomerService;
import de.fhwedel.pimpl.data.service.OfferService;
import de.fhwedel.pimpl.view.MainView;
import de.fhwedel.pimpl.view.contract.component.ContractForm;
import de.fhwedel.pimpl.view.contract.component.ContractStatusDropdown;
import de.fhwedel.pimpl.view.contract.component.ContractStatusLabel;
import de.fhwedel.pimpl.view.customer.component.CustomerForm;
import de.fhwedel.pimpl.view.offer.component.OfferForm;
import de.fhwedel.pimpl.view.offer.component.OfferSelection;
import de.fhwedel.pimpl.view.shared.TwoSideToolbar;
import de.fhwedel.pimpl.view.shared.notification.ErrorNotification;
import de.fhwedel.pimpl.view.shared.notification.SuccessNotification;
import de.fhwedel.pimpl.view.shared.notification.WarningNotification;

/**
 * The Detail View of a Contract, needed to edit and view a Contract Object
 * 
 * @author Lennard Trippensee (imca103936)
 *
 */
@Route(value = "contract", layout = MainView.class)
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class ContractDetailView extends VerticalLayout implements HasUrlParameter<Integer> {
	private ContractService contr_service;
	private CustomerService cust_service;
	private OfferService offer_service;
	private Optional<Contract> contract;

	private ContractForm contr_form;
	private CustomerForm cust_form;
	private OfferSelection offer_selec;
	private ContractStatusLabel status_label = new ContractStatusLabel();

	private ContractStatusDropdown status_dropdown;

	// first button menu
	private Button delete_btn = new Button("Löschen", this::onContrDeleteClick);
	private Button save_btn = new Button("Speichern", this::onContrSaveClick);
	private Button cancel_btn = new Button("Abbrechen", this::onContrCancelClick);
	private Button send_confirmation_btn = new Button("Auftragsbestätigung senden", this::onSendConfirmationClick);
	private Button skip_to_start_date_btn = new Button("Zum Vertragsbeginn springen", this::onSkipToStartClick);

	private Button set_supervise_mode = new Button("Supervise Mode", this::onSuperviseClick);
	private HorizontalLayout button_ctl;

	private Button contr_install_date_update = new Button("Installationstermine setzten",
			this::onInstallDatesUpdatedPressed);
	private Button cust_credit_update = new Button("Bonität abfragen", this::onCreditUpdatedPressed);

	/**
	 * This Consumer is used to keep the Components in sync. is Called, when a child
	 * component changes the Customer object spreads the change to other components.
	 * Habe dies verwendet, und kein Runnable, um Datenbankabfragen zu sparen..
	 * 
	 */
	private Consumer<Contract> onContractChange = new Consumer<Contract>() {

		@Override
		public void accept(Contract t) {
			setContract(t);

		}
	};

	public ContractDetailView(ContractService contr_service, OfferService offer_service, CustomerService cust_service) {
		this.contr_service = contr_service;
		this.offer_service = offer_service;
		this.cust_service = cust_service;
		this.status_dropdown = new ContractStatusDropdown(contr_service, cust_service);
		this.button_ctl = new HorizontalLayout(set_supervise_mode, delete_btn, status_dropdown, save_btn, cancel_btn);
		this.contr_form = new ContractForm(contr_service, true, false, true);
		this.offer_selec = new OfferSelection(this.offer_service, this.contr_service);
		this.cust_form = new CustomerForm(cust_service, true);
	}

	@Override
	public void setParameter(BeforeEvent event, Integer parameter) {
		if (parameter != null) {
			this.removeAll();
			this.contract = contr_service.findContractById(parameter);
			if (this.contract.isPresent()) {
				add(setToolbar());
				showContract();
				showOffer();
				showCustomer();
				setContract(this.contract.get());
			} else {
				add(new Label("NO CONTRACT WITH THIS ID FOUND."));
			}
		}
	}

	/**
	 * Spreads the Contract Object to all sub-components:
	 */
	private void setContract(Contract c) {
		assert c != null;
		this.contract = Optional.of(c);
		if (this.contract.isPresent()) {
			this.contr_form.setContract(this.contract.get());
			this.cust_form.setCustomer(this.contract.get().getCust());
			this.status_label.setStatus(this.contract.get().getStatus());
			this.status_dropdown.setContract(this.contract.get());
			this.offer_selec.setContract(this.contract.get());
			setOrderMode();
		}
	}

	private void showContract() {
		H3 title = new H3("Vertrag");
		title.getStyle().set("margin", "0em");

		this.contr_install_date_update.addThemeVariants(ButtonVariant.LUMO_SMALL);
		this.send_confirmation_btn.addThemeVariants(ButtonVariant.LUMO_SMALL);
		this.skip_to_start_date_btn.addThemeVariants(ButtonVariant.LUMO_SMALL);
		HorizontalLayout content = new HorizontalLayout(contr_install_date_update, send_confirmation_btn,
				skip_to_start_date_btn);
		add(new VerticalLayout(new TwoSideToolbar(new VerticalLayout(title), content), this.contr_form));
	}

	private void showCustomer() {
		H3 title = new H3("Kunde");
		title.getStyle().set("margin", "0em");
		this.cust_credit_update.addThemeVariants(ButtonVariant.LUMO_SMALL);
		HorizontalLayout content = new HorizontalLayout(cust_credit_update);
		add(new VerticalLayout(new TwoSideToolbar(new VerticalLayout(title), content), this.cust_form));
	}

	private void showOffer() {
		this.add(this.offer_selec);
	}

	private TwoSideToolbar setToolbar() {
		// Set the Title + Status:
		H1 title = new H1("Vertragsübersicht");
		Style title_style = title.getStyle();
		title_style.set("margin", "0em");
		VerticalLayout v1 = new VerticalLayout(title, status_label);
		v1.setSizeUndefined();

		// Set the Buttons to edit and delete & set status
		delete_btn.addThemeVariants(ButtonVariant.LUMO_ERROR);

		this.status_dropdown.listenOnValueChange(this.onContractChange);

		button_ctl.setDefaultVerticalComponentAlignment(Alignment.END);
		button_ctl.setSizeUndefined();
		cancel_btn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

		setToolbarEditMode(false);
		return new TwoSideToolbar(v1, button_ctl);
	}

	private void onCreditUpdatedPressed(com.vaadin.flow.component.ClickEvent<Button> event) {
		if (this.contract.isPresent()) {
			this.cust_form.saveCreditIndex();
			this.setContract(this.contract.get());
		}
	}

	private void setOrderMode() {
		if (this.contract.isPresent()) {
			boolean isOrderStatus = this.contract.get().getStatus() == ContractStatus.ORDER;
			contr_install_date_update.setVisible(!this.contract.get().getSelfInstallation() && isOrderStatus);
			send_confirmation_btn.setVisible(isOrderStatus && this.contr_service.checkInstallDates(this.contract.get()));
			skip_to_start_date_btn.setVisible(isOrderStatus && this.contr_service.checkConfirmationSent(this.contract.get()));
		}
	}

	private void setSuperviseMode(boolean activate) {
		setToolbarEditMode(activate);

		// set the form:
		this.contr_form.setSuperviseMode(activate);
		this.offer_selec.setSuperviseMode(activate);
	}

	private void setToolbarEditMode(boolean activate) {
		// swap the buttons
		this.cancel_btn.setVisible(activate);
		this.save_btn.setVisible(activate);

		this.delete_btn.setVisible(!activate);
		this.set_supervise_mode.setVisible(!activate);
		this.status_dropdown.setVisible(!activate);

		if (activate) {
			this.send_confirmation_btn.setVisible(!activate);
			this.skip_to_start_date_btn.setVisible(!activate);
			this.contr_install_date_update.setVisible(!activate);
		} else {
			setOrderMode();
		}
	}

	private void onContrDeleteClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		contract.ifPresent(c -> {
			Dialog d = new Dialog();
			d.setCloseOnEsc(true);
			d.add(new Label("Vertrag wirklich löschen?"));
			d.add(new HorizontalLayout(new Button("Ja, wirklich", ev -> {
				d.close();
				contr_service.removeContract(c);
				new SuccessNotification("Vertrag erfolgreich gelöscht!").open();
				if (UI.getCurrent() != null) {
					UI.getCurrent().navigate("manage/contracts/");
				}
			}), new Button("Oops, lieber doch nicht", ev -> d.close())));
			d.open();
		});
	}

	private void onSuperviseClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		setSuperviseMode(true);
	}

	private void onContrSaveClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		Optional<Contract> contr_valid = contr_form.saveContract();
		if (contr_valid.isPresent()) {
			this.setContract(contr_valid.get());
			setSuperviseMode(false);
		}
	}

	private void onContrCancelClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		this.contr_form.refresh();
		setSuperviseMode(false);
	}

	private void onInstallDatesUpdatedPressed(com.vaadin.flow.component.ClickEvent<Button> event) {
		this.contr_form.setInstallationFieldsReadOnly(false);
		this.setToolbarEditMode(true);
	}

	private void onSendConfirmationClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		if (contract.isPresent()) {
			if (!contract.get().getConfirmationSend()) {
				this.contract.get().setConfirmationSend(true);
				this.setContract(this.contract.get());
				Optional<Contract> contr_valid = contr_form.saveContract();
				if (contr_valid.isPresent()) {
					new SuccessNotification("Buchungsbestätigung versendet!").open();
				}
			} else {
				new SuccessNotification("Buchungsbestätigung wurde erneut versendet!").open();
			}
		}
	}

	private void onSkipToStartClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		this.status_dropdown.setSkipStartDateCheck(true);
		new SuccessNotification("Die Zukunfts-Prüfung wurde erfolgreich aufgehoben.").open();
	}

}
