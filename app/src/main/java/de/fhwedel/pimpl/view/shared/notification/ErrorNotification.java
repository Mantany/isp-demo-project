package de.fhwedel.pimpl.view.shared.notification;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

/**
 * A custom error Notification Component
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class ErrorNotification extends Notification {
	public ErrorNotification(String text) {
		this.setText("❌ "+ text);
		this.setDuration(6000);
	}
}
