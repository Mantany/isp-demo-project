package de.fhwedel.pimpl.view.contract;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.service.ContractService;
import de.fhwedel.pimpl.data.service.CustomerService;
import de.fhwedel.pimpl.view.MainView;
import de.fhwedel.pimpl.view.contract.component.ContractForm;
import de.fhwedel.pimpl.view.shared.TwoSideToolbar;
import de.fhwedel.pimpl.view.shared.notification.ErrorNotification;

/**
 * The page to add new Contracts, its possible to add the customer ID in order
 * to Automaticly link the customer with the new Contract -> Customer ID is
 * nessesary!
 * 
 * @author Lennard Trippensee (imca103936)
 *
 */
@Route(value = "contracts/add", layout = MainView.class)
@PageTitle("Vertrag Hinzufügen | ISP Management Portal")
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class ContractAddView extends VerticalLayout implements BeforeEnterObserver {
	private Contract contract;
	private Optional<Customer> customer = Optional.empty();

	private CustomerService cust_service;
	private ContractService contr_service;
	private ContractForm contr_form;

	private Button save_btn = new Button("Speichern", this::onContrSaveClick);
	private Button cancel_btn = new Button("Abbrechen", this::onContrCancelClick);

	public ContractAddView(ContractService contr_service, CustomerService cust_service) {
		this.contr_service = contr_service;
		this.cust_service = cust_service;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		// get the query Params:
		Location loc = event.getLocation();
		QueryParameters queryParameters = loc.getQueryParameters();
		Map<String, List<String>> parametersMap = queryParameters.getParameters();

		if (parametersMap.containsKey("cust_id")) {
			this.removeAll();
			try {
				Integer cust_id = Integer.parseInt(parametersMap.get("cust_id").get(0));
				this.customer = cust_service.findCustomerById(cust_id);
				if (this.customer.isEmpty()) {
					new ErrorNotification("Kein Customer mit der ID gefunden.").open();
				} else {
					// create new contract
					this.contract =  contr_service.generateContract();
					this.contract.setCust(this.customer.get());
					this.contr_form = new ContractForm(contr_service, false, true, false);
					this.contr_form.setReadOnly(false);
					this.contr_form.setContract(this.contract);
					this.contr_form.setFilledFieldsReadOnly(true);
					this.add(setToolbar());
					this.add(contr_form);
				}
			} catch (NumberFormatException e) {
				new ErrorNotification("Die Customer ID muss eine Zahl sein.").open();
			}

			if (this.customer.isEmpty()) {
				this.removeAll();
				this.add(new H1("Bitte einen Kunden angeben!"));
			}
		} else {
			this.removeAll();
			this.add(new H2("Es wurde kein Kunde ausgewählt. Bitte wählen sie einen Kunden und klicken sie auf Vertrag anlegen."));
		}

	}
	
	
	private TwoSideToolbar setToolbar() {
		save_btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		cancel_btn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		HorizontalLayout button_ctl = new HorizontalLayout(save_btn, cancel_btn);
		button_ctl.setDefaultVerticalComponentAlignment(Alignment.END);
		button_ctl.setSizeUndefined();

		return new TwoSideToolbar("Vertrag Anlegen", button_ctl);
	}

	/**
	 * Checks the UI validation, and the server side validation. If everything
	 * correct, saves the customer. else throws error messages
	 * 
	 * @param event
	 */
	private void onContrSaveClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		Optional<Contract> contr_valid = contr_form.saveContract();
		if(contr_valid.isPresent()) {
			this.contract = contr_valid.get();
			if (UI.getCurrent() != null) {
				UI.getCurrent().navigate("manage/contract/" + this.contract.getId());
			}
		}
	}

	private void onContrCancelClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		if (UI.getCurrent() != null) {
			UI.getCurrent().navigate("manage/contracts/");
		}
	}

}
