package de.fhwedel.pimpl.view.offer.component;

import java.util.List;
import java.util.Optional;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Availability;
import de.fhwedel.pimpl.data.model.Offer;


/**
 * Shows the Offer with the Availability
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class OfferForm extends Composite<Component>{
	private Optional<Offer> offer = Optional.empty();
	private Binder<Offer> binder = new BeanValidationBinder<>(Offer.class);
	
	private TextField description = new TextField("Beschreibung");
	private IntegerField minDuration = new IntegerField("min. Vertragslänge");
	private IntegerField bandwidthDownlink = new IntegerField("Download Geschwindigkeit in kbit/s");
	private IntegerField bandwidthUplink = new IntegerField("Upload Geschwindigkeit in kbit/s");
	private IntegerField monthPrice = new IntegerField("Monatspreis in Cent");
	
	private Grid<Availability> avail_grid = new Grid<Availability>();
	private boolean show_avail;
	
	private VerticalLayout view = new VerticalLayout();
	
	
	public OfferForm(boolean show_avail) {
		this.show_avail = show_avail;
		binder.bindInstanceFields(this);
		view.add(new FormLayout(monthPrice, bandwidthUplink, bandwidthDownlink, minDuration, description));
		this.setReadOnly(true);
		
		if(show_avail) {
			configureGrid();
			view.add(new H4("Verfügbar in"), avail_grid);
		}
		
	}
	
	@Override
	protected Component initContent() {
		return view;
	}
	
	private void setReadOnly(boolean readonly) {
		description.setReadOnly(readonly);
		minDuration.setReadOnly(readonly);
		bandwidthDownlink.setReadOnly(readonly);
		bandwidthUplink.setReadOnly(readonly);
		monthPrice.setReadOnly(readonly);
	}
	
	
	public Optional<Offer> saveOffer() {
		if(offer.isPresent() && binder.writeBeanIfValid(offer.get())) {
			return this.offer;
		}
		return Optional.empty();
	}
	
	public void refresh() {
		if(offer.isPresent()) {
			binder.readBean(offer.get());
			if(show_avail) {
				avail_grid.setDataProvider(new ListDataProvider<Availability>(this.offer.get().getAvail()));		
			}
		}
	}
	
	private void configureGrid() {
		// Set up the grid:
		avail_grid.setSizeFull();
		avail_grid.setHeight("300px");
		
		avail_grid.addColumn(Availability::getStartPlz).setHeader("Start PLZ").setSortable(true);
		avail_grid.addColumn(Availability::getEndPlz).setHeader("End PLZ").setSortable(true);
		avail_grid.setSelectionMode(SelectionMode.SINGLE);

		avail_grid.getColumns().forEach(col -> col.setAutoWidth(true));
	}
	
	public void setOffer(Optional<Offer> offer) {
		this.offer = offer;
		refresh();
	}
	
	public Optional<Offer> getOffer(){
		return this.offer;
	}
	
}
