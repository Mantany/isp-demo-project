package de.fhwedel.pimpl.view.contract.component;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Contract.ContractStatus;
import de.fhwedel.pimpl.data.service.ContractService;
import de.fhwedel.pimpl.data.service.CustomerService;
import de.fhwedel.pimpl.view.shared.notification.ErrorNotification;
import de.fhwedel.pimpl.view.shared.notification.WarningNotification;

/**
 * Component to show and change the Status of a contract element + checking the
 * business rules
 * 
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class ContractStatusDropdown extends Composite<Component> {
	private ContractService contr_service;
	private CustomerService cust_service;
	private Contract contract;
	private boolean skip_start_date_check = false;

	private Select<ContractStatus> select = new Select<Contract.ContractStatus>();
	private Optional<Consumer<Contract>> onValueChangeListener;

	public void listenOnValueChange(Consumer<Contract> listener) {
		this.onValueChangeListener = Optional.of(listener);
	}

	public ContractStatusDropdown(ContractService contr_service, CustomerService cust_service) {
		this.contr_service = contr_service;
		this.cust_service = cust_service;
		this.select.addValueChangeListener(this::onValueChanged);
	}

	public void setContract(Contract c) {
		this.contract = c;
		setAllowedItems();
		this.select.setValue(contract.getStatus());
	}

	@Override
	protected Component initContent() {
		return select;
	}

	private void onValueChanged(ComponentValueChangeEvent<Select<ContractStatus>, ContractStatus> event) {
		if (event.getValue() != null && event.isFromClient()) {

			boolean valid = true;

			switch (event.getValue()) {
			case REJECTBONI:
				valid = checkStatusRejectBoni();
				// it should not be possible to reject if the credit score is good.
				if (this.cust_service.checkCreditDate(this.contract.getCust())
						&& this.cust_service.checkCreditScore(this.contract.getCust())) {
					new ErrorNotification("Der Bonitätsindex ist gültig. Ablehnung nicht möglich.").open();
					valid = false;
				}
				break;
			case ORDER:
				valid = checkStatusOrder();
				break;
			case ACTIVE:
				valid = checkStatusActive();
				break;
			default:
				break;
			}

			if (valid) {
				this.contract.setStatus(event.getValue());
				// Business-Rule
				if (this.contract.getStatus() == ContractStatus.ACTIVE
						&& this.contr_service.startDateIsOverlappingWithInstalledOnDate(this.contract)) {
					this.contract.setStartDate(this.contract.getInstalledOnDate());
					new WarningNotification("Achtung: Vertragsbeginn wurde verändert!").open();
				}
				this.contract.setActivatedOnDate(this.contract.getStartDate());

				Optional<Contract> service_eval = contr_service.saveContract(this.contract);

				if (service_eval.isPresent()) {
					this.setContract(service_eval.get());
					this.onValueChangeListener.ifPresent(r -> {
						r.accept(contract);
					});
				} else {
					new ErrorNotification("Es gab einen Fehler bei der Statusänderung.").open();
				}
			} else {
				this.select.setValue(event.getOldValue());
			}

		}
	}

	/**
	 * Validation Gate of Status "Reject Boni"
	 * 
	 * @return true = valid, false = not valid
	 */
	private boolean checkStatusRejectBoni() {
		boolean valid = true;
		if (!this.contr_service.checkOffer(contract)) {
			new ErrorNotification("Bitte ein gültiges Angebot hinterlegen").open();
			valid = false;
		}
		return valid;
	}

	private boolean checkStatusOrder() {
		boolean valid = true;
		if (!this.contr_service.checkOffer(contract)) {
			new ErrorNotification("Bitte ein gültiges Angebot hinterlegen").open();
			valid = false;
		} else if (!this.cust_service.checkCreditDate(this.contract.getCust())) {
			new ErrorNotification("Der Bonitätsindex ist zu alt. Bitte neuen anfordern.").open();
			valid = false;
		} else if (!this.cust_service.checkCreditScore(this.contract.getCust())) {
			new ErrorNotification("Der Bonitätsindex ist zu hoch. <= 300").open();
			valid = false;
		}
		return valid;
	}

	private boolean checkStatusActive() {
		// same as the order:
		boolean valid = checkStatusOrder();

		// and two new checks:
		if (!this.contr_service.checkInstallDates(this.contract)) {
			new ErrorNotification("Bitte die Installationsdaten angeben!").open();
			valid = false;
		} else if (!this.contr_service.checkConfirmationSent(this.contract)) {
			new ErrorNotification("Es wurde noch keine Vertragsbestätigung an den Kunden versendet.").open();
			valid = false;
		} else if (!this.contr_service.checkStartDateForActivation(this.contract) && !this.skip_start_date_check) {
			new ErrorNotification("Der Vertragsbeginn darf nicht in der Zukunft liegen.").open();
			valid = false;
		}
		return valid;
	}

	private void setAllowedItems() {
		List<ContractStatus> allowed_choices = contr_service.getAllowedContractStatus(this.contract);
		this.select.setItems(allowed_choices);
	}

	/**
	 * Sets if the start date should be checked when status is set to "Active"
	 * Business-Check wird hiermit ausgeschaltet, um zu erlauben, "in der Zeit zu
	 * springen"
	 * 
	 * @param skip
	 */
	public void setSkipStartDateCheck(boolean skip) {
		this.skip_start_date_check = skip;
	}

}
