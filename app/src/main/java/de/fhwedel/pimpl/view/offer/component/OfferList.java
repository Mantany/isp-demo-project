package de.fhwedel.pimpl.view.offer.component;


import java.util.Optional;
import java.util.function.Consumer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.ItemDoubleClickEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.component.textfield.TextField;

import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.model.Offer;
import de.fhwedel.pimpl.data.service.OfferService;

/**
 * A Component to show Offers in a List
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class OfferList extends Composite<Component> {
	private OfferService offer_service;
	private Grid<Offer> grid = new Grid<Offer>();
	private boolean supervise_mode = false;
	
	private VerticalLayout view = new VerticalLayout();
	private Optional<Consumer<Offer>> onSelectionDoubleClickListener = Optional.empty();
	private Optional<Consumer<Offer>> onSelectionClickListener = Optional.empty();
	
	
	private Customer filter_by_cust = null;
	
	public OfferList(OfferService offer_service) {
		this.offer_service = offer_service;
		this.view.setSizeFull();
		configureGrid();
		this.view.add(grid);
		updateList();
		
	}
	
	public void setCustomer(Customer filter) {
		this.filter_by_cust = filter;
		updateList();
	}
	
	@Override
	protected Component initContent() {
		return view;
	}

	private void configureGrid() {
		// Set up the grid:
		grid.setSizeFull();
		grid.removeAllColumns();

		grid.addColumn(Offer::getBandwidthDownlink).setHeader("Downloadrate").setSortable(true);
		grid.addColumn(Offer::getBandwidthUplink).setHeader("Uploadrate").setSortable(true);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setHeight("300px");

		grid.addItemDoubleClickListener(this::onSelectionDoubbleClickListener);
		grid.addItemClickListener(this::onSelectionClickListener);
		grid.getColumns().forEach(col -> col.setAutoWidth(true));
	}
	
	public void setSupervisedMode(boolean supervise_mode) {
		this.supervise_mode = supervise_mode;
		updateList();
	}
	
	private void onSelectionDoubbleClickListener(ItemDoubleClickEvent<Offer> event) {
		this.onSelectionDoubleClickListener.ifPresent(r -> {r.accept(event.getItem());});
	}
	
	public void listenToDoubleClick(Consumer<Offer> listener) {
		this.onSelectionDoubleClickListener = Optional.of(listener);	
	}
	
	private void onSelectionClickListener(ItemClickEvent<Offer> event) {
		this.onSelectionClickListener.ifPresent(r -> {r.accept(event.getItem());});
	}
	
	public void listenToClick(Consumer<Offer> listener) {
		this.onSelectionClickListener = Optional.of(listener);	
	}
	
	private void updateList() {
		if(filter_by_cust == null || this.supervise_mode) {
			grid.setDataProvider(new ListDataProvider<Offer>(offer_service.findAllOffers()));
		} else {
			grid.setDataProvider(new ListDataProvider<Offer>(offer_service.findAllOffersByCustomer(filter_by_cust)));
		}
	}
	
	

}
