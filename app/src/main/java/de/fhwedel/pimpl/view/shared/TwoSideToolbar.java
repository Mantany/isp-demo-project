package de.fhwedel.pimpl.view.shared;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.orderedlayout.FlexLayout.WrapMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

/**
 * Basic UI Element for a Toolbar with Title and customizable right component.
 * @author Mr.Wuppi
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class TwoSideToolbar extends Composite<Component>{
	
	private FlexLayout toolbar = new FlexLayout();
	private Component[] right;
	private VerticalLayout left = new VerticalLayout();
	
	
	/**
	 * Toolbar with Title
	 * @param title
	 * @param right
	 */
	public TwoSideToolbar(String title, Component... right) {
		// Set the Title:
		H1 titleLabel = new H1(title);
		titleLabel.getStyle().set("margin", "0em");
		this.left.add(titleLabel);
		this.right = right;
		setToolbar();
	}
	
	/**
	 * Toolbar setup without title and custom Components left + right
	 * @param left
	 * @param right
	 */
	public TwoSideToolbar(VerticalLayout left, Component... right) {
		this.left = left;
		this.right = right;
		setToolbar();
	}
	
	
	@Override
	protected Component initContent() {
		return toolbar;
	}
	
	private void setToolbar() {
		
		left.setSizeUndefined();

		// Put everything inside a FlexLayout:
		toolbar.add(this.left);
		toolbar.add(this.right);
		toolbar.setJustifyContentMode(JustifyContentMode.BETWEEN);
		toolbar.setWidthFull();
		toolbar.setAlignItems(Alignment.CENTER);
		toolbar.setWrapMode(WrapMode.WRAP);
	}
	
}
