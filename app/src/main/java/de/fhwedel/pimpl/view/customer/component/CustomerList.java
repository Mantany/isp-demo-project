package de.fhwedel.pimpl.view.customer.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.ItemDoubleClickEvent;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.service.CustomerService;


@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class CustomerList extends Composite<Component>  {
	private Grid<Customer> grid = new Grid<>(Customer.class);
	private TextField search_text = new TextField();
	private Button search_btn = new Button();
	private HorizontalLayout search_bar = new HorizontalLayout();
	private CustomerService cust_service;
	
	
	private VerticalLayout view = new VerticalLayout();
	
	public CustomerList(CustomerService cust_service) {
		this.cust_service = cust_service;
		this.view.setSizeFull();
		configureGrid();
		configureSearchBar();
		this.view.add(search_bar);
		this.view.add(grid);
		updateList();
	}
	
	@Override
	protected Component initContent() {
		return view;
	}

	private void configureSearchBar() {
		search_text.setPlaceholder("Name, Vorname oder Kundennummer eingeben");
		search_text.setClearButtonVisible(true);
		search_text.setValueChangeMode(ValueChangeMode.LAZY);
		search_text.setPrefixComponent(VaadinIcon.SEARCH.create());
		search_text.setWidth("400px");
		
		search_btn.setText("Suchen");
		search_btn.addClickListener(e -> updateList());
		
		search_bar.add(search_text, search_btn);
	}

	private void configureGrid() {
		// Set up the grid:
		grid.setSizeFull();
		grid.removeAllColumns();

		grid.addColumn(Customer::getCustnum).setHeader("Kundennummer").setSortable(true);
		grid.addColumn(Customer::getSurname).setHeader("Name").setSortable(true);
		grid.addColumn(Customer::getPrename).setHeader("Vorname").setSortable(true);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setHeight("500px");

		grid.addItemDoubleClickListener(this::onCustNavigate);
		grid.getColumns().forEach(col -> col.setAutoWidth(true));
	}
	
	private void onCustNavigate(ItemDoubleClickEvent<Customer> event) {
		if(UI.getCurrent() != null) {
			UI.getCurrent().navigate("manage/customer/" + event.getItem().getId());
		}
	}
	
	public void updateList() {
		grid.setDataProvider(new ListDataProvider<Customer>(cust_service.findAllCustomer(search_text.getValue())));
	}
	
	
	
}
