package de.fhwedel.pimpl.view.customer.component;

import java.util.Optional;
import java.util.function.Consumer;

import com.fasterxml.jackson.datatype.jdk8.OptionalDoubleSerializer;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Address;
import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.model.Offer;
import de.fhwedel.pimpl.data.service.CustomerService;

/**
 *
 * 
 * @author Mr.Wuppi
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class AddressEditList extends Composite<Component> {
	private CustomerService cust_service;
	private Optional<Customer> customer = Optional.empty();
	private Optional<Address> addr = Optional.empty();
	private Binder<Address> binder = new BeanValidationBinder<>(Address.class);

	private Grid<Address> grid = new Grid<Address>(Address.class);

	private TextField street = new TextField();
	private TextField zip = new TextField();
	private TextField city = new TextField();
	private FormLayout addr_form = new FormLayout();

	private Button addr_new = new Button("Neue Adresse", this::onAddrNewClick);
	private Button addr_safe = new Button("Adresse sichern", this::onAddrSafeClick);
	private Button addr_reset = new Button("Reset", this::onAddrResetClick);
	private Button addr_delete = new Button("Adresse löschen", this::onAddrDeleteClick);
	private HorizontalLayout addr_ctrl = new HorizontalLayout(new VerticalLayout(addr_new, addr_safe),
			new VerticalLayout(addr_reset, addr_delete));

	private VerticalLayout view = new VerticalLayout();

	private Optional<Consumer<Customer>> onChangeListener = Optional.empty();

	@Override
	protected Component initContent() {
		return view;
	}

	public AddressEditList(CustomerService cust_service) {
		this.cust_service = cust_service;
		this.customer = customer;
		configureGrid();
		configureForm();
		this.binder.bindInstanceFields(this);

		this.view.add(grid, addr_form, addr_ctrl);

		refreshForm();
		refreshGrid();
	}

	public void setCustomer(Customer c) {
		this.customer = Optional.of(c);
		refreshForm();
		refreshGrid();
	}

	public void listenToCustomerChange(Consumer<Customer> r) {
		this.onChangeListener = Optional.of(r);
	}

	private void onCustomerChange() {
		if (customer.isPresent()) {
			this.onChangeListener.ifPresent(r -> {
				r.accept(customer.get());
			});
		}
	}

	private void configureForm() {
		this.addr_delete.addThemeVariants(ButtonVariant.LUMO_ERROR);

		this.addr_form.addFormItem(street, "Straße");
		this.addr_form.addFormItem(zip, "PLZ");
		this.addr_form.addFormItem(city, "Ort");
	}

	private void configureGrid() {
		// Set up the grid:
		grid.removeColumnByKey("cust");
		grid.removeColumnByKey("id");

		grid.setSizeFull();
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setHeight("300px");

		grid.addSelectionListener(this::onCustNavigate);
		grid.getColumns().forEach(col -> col.setAutoWidth(true));
	}

	private void onCustNavigate(SelectionEvent<Grid<Address>, Address> event) {
		setAddress(event.getFirstSelectedItem());
	}

	private void refreshForm() {
		binder.readBean(addr.orElse(null));
		addr_form.setEnabled(addr.isPresent());
		addr_safe.setEnabled(addr.isPresent());
		addr_reset.setEnabled(addr.isPresent());
		addr_delete.setEnabled(addr.map(a -> a.getId() != null).orElse(false));
	}

	private void refreshGrid() {
		if (customer.isPresent()) {
			grid.setDataProvider(new ListDataProvider<Address>(this.customer.get().getAddresses()));
		}
	}

	private void setAddress(Optional<Address> addr) {
		this.addr = addr;
		refreshForm();
	}

	private void onAddrNewClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		addr = Optional.of(new Address());
		refreshForm();
	}

	private void onAddrSafeClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		if (customer.isPresent()) {
			addr.ifPresent(a -> {
				if (binder.writeBeanIfValid(a)) {
					this.setAddress(cust_service.saveAddress(a, this.customer.get()));
					refreshGrid();
					onCustomerChange();
				}
			});
		}
	}

	private void onAddrResetClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		setAddress(addr);
	}

	private void onAddrDeleteClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		if (customer.isPresent()) {
			addr.ifPresent(a -> {
				Dialog d = new Dialog();
				d.setCloseOnEsc(true);
				d.add(new Label("Adresse wirklich löschen?"));
				d.add(new HorizontalLayout(new Button("Ja, wirklich", ev -> {
					d.close();
					cust_service.deleteAddress(a);
					refreshForm();
					refreshGrid();
					onCustomerChange();
				}), new Button("Oops, lieber doch nicht", ev -> d.close())));
				d.open();
			});
		}
	}
}
