package de.fhwedel.pimpl.view.customer.component;

import java.util.Optional;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.PropertyId;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.model.Customer.Salutation;
import de.fhwedel.pimpl.data.service.CustomerService;
import de.fhwedel.pimpl.view.shared.notification.ErrorNotification;
import de.fhwedel.pimpl.view.shared.notification.SuccessNotification;

/**
 * The Customer form to display a Contract. Only UI Elements
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class CustomerForm extends Composite<Component>  {
	private Optional<Customer> customer = Optional.empty();
	private CustomerService cust_service;
	private Binder<Customer> binder = new BeanValidationBinder<>(Customer.class);
	private boolean show_credit;
	private boolean supervise_mode;
	
	// The fields:
	@PropertyId("custnum")
	private TextField cnum = new TextField("Kundennummer");
	private ComboBox<Salutation> salutation = new ComboBox<>("Geschlecht", Salutation.values());
	private TextField surname = new TextField("Nachname");
	private TextField prename = new TextField("Vorname");
	private IntegerField creditIndex = new IntegerField("Bonitätsindex");
	private DatePicker creditUpdatedDate = new DatePicker("Bonität aktualisiert am");
	
	
	private VerticalLayout view = new VerticalLayout();
	
	@Override
	protected Component initContent() {
		return view;
	}
	
	/**
	 * Constructor for a Customer Form.
	 * @param cust_service Connection to the service in order to vaildate and save stuff to database
	 * @param show_credit wether the credit section of a customer should be visible
	 */
	public CustomerForm(CustomerService cust_service, boolean show_credit) {
		this.show_credit = show_credit;
		this.cust_service = cust_service;
		binder.bindInstanceFields(this);
		//Cust.num && creditscore is allways read-only (except supervise)
		cnum.setReadOnly(true);
		creditIndex.setReadOnly(true);
		creditUpdatedDate.setReadOnly(true);
		this.setReadOnly(true);
		
		view.add(new FormLayout(cnum, prename, surname, salutation));
		if(show_credit) {
			view.add(new H4("Bonität"), new FormLayout(creditIndex, creditUpdatedDate));
		}
	}
	
	public void setReadOnly(boolean readonly) {
		prename.setReadOnly(readonly);
		surname.setReadOnly(readonly);
		salutation.setReadOnly(readonly);
	}
	
	public void setFilledFieldsReadOnly(boolean readonly) {
		if(customer.isPresent()) {
			if(customer.get().getPrename() != null) {
				prename.setReadOnly(readonly);
			}
			if(customer.get().getSurname() != null) {
				surname.setReadOnly(readonly);
			}
			if(customer.get().getSalutation() != null) {
				salutation.setReadOnly(readonly);
			}
		}
	}
	
	/**
	 * Validates and saves the Customer-Instance to the database.
	 * @return Empty Optional if something went wrong, else the new customer instance.
	 */
	public Optional<Customer> saveCustomer() {
		if(customer.isPresent() && binder.writeBeanIfValid(customer.get())) {
			//TODO check business rules only when supervised off is. right now: no business rules..
			Optional<Customer> contr_business_valid = cust_service.saveCustomer(customer.get());
			if (contr_business_valid.isPresent()) {
				this.setCustomer(contr_business_valid.get());
				new SuccessNotification("Änderungen gespeichert!").open();
				return this.customer;
			} else {
				new ErrorNotification("Es ist ein Fehler beim erstellen des Kunden aufgetreten.").open();
			}
		} else {
			new ErrorNotification("Bitte alle Felder korrekt ausfüllen").open();
		}

		return Optional.empty();
	}
	
	public void refresh() {
		binder.readBean(customer.orElse(null));
	}
	
	public void setCustomer(Customer customer) {
		this.customer = Optional.of(customer);
		refresh();
	}
	
	public Optional<Customer> getCustomer(){
		return this.customer;
	}
	
	/**
	 * Sets the Supervisemode
	 * @param s true/false superviisedmode
	 */
	public void setSuperviseMode(boolean s) {
		prename.setReadOnly(!s);
		surname.setReadOnly(!s);
		salutation.setReadOnly(!s);
		creditIndex.setReadOnly(!s);
		creditUpdatedDate.setReadOnly(!s);
	}
	
	
	
	/**
	 * This method is used to query a request to a credit-institute and change the Credit Index + date accordingly
	 * For now, this is only a mock-up
	 */
	public void saveCreditIndex() {
		if(customer.isPresent()) {
			Optional<Customer> credit_change = cust_service.saveCreditIndex(this.customer.get());
			if(credit_change.isPresent()) {
				this.setCustomer(credit_change.get());
				//TODO Bescheid geben!
				new SuccessNotification("Bonitätsindex erfolgreich angefragt!").open();
			} else {
				new ErrorNotification("Bei der Erhebung des Indexes ist leider ein Fehler aufgetreten!").open();
			}
			
		}
	}
	
}
