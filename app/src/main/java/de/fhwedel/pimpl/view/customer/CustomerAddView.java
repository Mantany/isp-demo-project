package de.fhwedel.pimpl.view.customer;

import java.util.Optional;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.service.CustomerService;
import de.fhwedel.pimpl.view.MainView;
import de.fhwedel.pimpl.view.customer.component.CustomerForm;
import de.fhwedel.pimpl.view.shared.TwoSideToolbar;

/**
 * The view for Customer adding
 * Gets a raw Customer Instance from the customer service and displays it in a customer form component.
 * @author Lennard Trippensee(imca103936)
 *
 */
@Route(value = "customers/add", layout = MainView.class)
@PageTitle("Kunde Hinzufügen | ISP Management Portal")
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class CustomerAddView extends VerticalLayout {
	private Customer customer;
	private CustomerService cust_service;
	private CustomerForm cust_form;

	private Button save_btn = new Button("Speichern", this::onCustSaveClick);
	private Button cancel_btn = new Button("Abbrechen", this::onCustCancelClick);


	public CustomerAddView(CustomerService cust_service) {
		this.removeAll();
		this.cust_service = cust_service;
		this.customer = cust_service.generateCustomer();
		this.cust_form = new CustomerForm(cust_service, false);
		this.cust_form.setReadOnly(false);
		this.cust_form.setCustomer(this.customer);
		this.cust_form.setFilledFieldsReadOnly(true);
		this.add(setToolbar());
		// this.contr_form.setContract(this.contract);
		this.add(cust_form);
	}

	private TwoSideToolbar setToolbar() {
		save_btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		cancel_btn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		HorizontalLayout button_ctl = new HorizontalLayout(save_btn, cancel_btn);
		button_ctl.setDefaultVerticalComponentAlignment(Alignment.END);
		button_ctl.setSizeUndefined();
		return new TwoSideToolbar("Kunde Anlegen", button_ctl);
	}

	/**
	 * Checks the UI validation, and the server side validation. If everything
	 * correct, saves the customer. else throws error messages
	 * 
	 * @param event
	 */
	private void onCustSaveClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		Optional<Customer> cust_valid = cust_form.saveCustomer();
		if (cust_valid.isPresent()) {
			this.customer = cust_valid.get();
			if (UI.getCurrent() != null) {
				UI.getCurrent().navigate("manage/customer/" + this.customer.getId());
			}
		}
	}

	private void onCustCancelClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		if (UI.getCurrent() != null) {
			UI.getCurrent().navigate("manage/customers/");
		}
	}

}
