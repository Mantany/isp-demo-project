package de.fhwedel.pimpl.view.shared.notification;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

/**
 * A custom success Notification Component
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class SuccessNotification extends Notification {
	public SuccessNotification(String text) {
		this.setText("🙌 " + text);
		this.setDuration(6000);
	}
}
