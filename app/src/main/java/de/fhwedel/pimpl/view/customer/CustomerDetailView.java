package de.fhwedel.pimpl.view.customer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.service.ContractService;
import de.fhwedel.pimpl.data.service.CustomerService;
import de.fhwedel.pimpl.data.service.OfferService;
import de.fhwedel.pimpl.view.MainView;
import de.fhwedel.pimpl.view.contract.component.ContractList;
import de.fhwedel.pimpl.view.customer.component.AddressEditList;
import de.fhwedel.pimpl.view.customer.component.CustomerForm;
import de.fhwedel.pimpl.view.offer.component.OfferList;
import de.fhwedel.pimpl.view.shared.TwoSideToolbar;
import de.fhwedel.pimpl.view.shared.notification.SuccessNotification;

/**
 * The Detail View of a Customer, needed to edit and view a Customer Object
 * 
 * @author Lennard Trippensee (imca103936)
 *
 */
@Route(value = "customer", layout = MainView.class)
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class CustomerDetailView extends VerticalLayout implements HasUrlParameter<Integer> {
	private CustomerService cust_service;
	private ContractService contr_service;
	private OfferService offer_service;
	private Optional<Customer> customer;

	private ContractList contr_list;
	private CustomerForm cust_form;
	private OfferList offer_list;
	private AddressEditList cust_adresses;
	private Tabs tabs;
	private Tab tab_detail;
	private Tab tab_contract;
	private Tab tab_offer;
	private boolean supervise_mode = false;
	
	
	
	/**
	 * This Consumer is used to keep the Components in sync.
	 * is Called, when a child component changes the Customer object 
	 * spreads the change to the differnen components.
	 * Habe dies verwendet, und kein Runnable, um Datenbankabfragen zu sparen..
	 * 
	 */
	private Consumer<Customer> onCustomerChange =  new Consumer<Customer>() {
		@Override
		public void accept(Customer t) {
			setCustomer(t);
		}
	};

	private Button edit_btn = new Button(new Icon(VaadinIcon.EDIT), this::onCustEditClick);
	private Button delete_btn = new Button("Löschen", this::onCustDeleteClick);
	private Button save_btn = new Button("Speichern", this::onCustSaveClick);
	private Button cancel_btn = new Button("Abbrechen", this::onCustCancelClick);
	private Button set_supervise_mode = new Button("Supervise Mode", this::onSuperviseClick);
	private HorizontalLayout button_ctl = new HorizontalLayout(edit_btn, delete_btn,set_supervise_mode, save_btn, cancel_btn);

	public CustomerDetailView(CustomerService cust_service, ContractService contr_service, OfferService offer_service) {
		this.contr_service = contr_service;
		this.cust_service = cust_service;
		this.offer_service = offer_service;
	}

	@Override
	public void setParameter(BeforeEvent event, Integer parameter) {
		if (parameter != null) {
			this.customer = cust_service.findCustomerById(parameter);
			showCustomer();
		}
	}

	private void showCustomer() {
		this.removeAll();
		if (this.customer.isPresent()) {
			this.add(setToolbar(customer.get()));
			addTabs(this.customer.get());
		} else {
			this.add(new H2("Kein Kunde mit dieser ID gefunden."));
		}
	}

	private void addTabs(Customer cust) {
		// Prepare first Tab:
		Tab tab_detail = new Tab("Details");
		VerticalLayout page1 = setDetailTabContent(cust);
		page1.setSizeFull();

		// Prepare second Tab:
		Tab tab_contract = new Tab("Verträge");
		VerticalLayout page2 = setContractTabContent(cust);
		page2.setVisible(false);

		// Prepare third Tab:
		Tab tab_offer = new Tab("Verfügbare Angebote");
		VerticalLayout page3 = setOfferTabContent(cust);
		page3.setVisible(false);

		// Mappen der Tabs:
		Map<Tab, Component> tabsToPages = new HashMap();
		tabsToPages.put(tab_detail, page1);
		tabsToPages.put(tab_contract, page2);
		tabsToPages.put(tab_offer, page3);
		Tabs tabs = new Tabs(tab_detail, tab_contract, tab_offer);
		Div pages = new Div(page1, page2, page3);
		pages.setSizeFull();

		tabs.addSelectedChangeListener(event -> {
			tabsToPages.values().forEach(page -> page.setVisible(false));
			Component selectedPage = tabsToPages.get(tabs.getSelectedTab());
			selectedPage.setVisible(true);
		});
		tabs.setSizeFull();

		add(tabs, pages);
		this.tabs = tabs;
		this.tab_detail = tab_detail;
		this.tab_contract = tab_contract;
		this.tab_offer = tab_offer;
	}

	private VerticalLayout setDetailTabContent(Customer cust) {
		cust_form = new CustomerForm(cust_service, true);
		cust_form.setCustomer(cust);
		this.add(cust_form);
		cust_adresses = new AddressEditList(cust_service);
		cust_adresses.setCustomer(cust);
		cust_adresses.listenToCustomerChange(this.onCustomerChange);
		this.add(cust_adresses);
		return new VerticalLayout(new H3("Stammdaten"), cust_form, new H3("Addressen"), cust_adresses);
	}

	private VerticalLayout setContractTabContent(Customer cust) {
		contr_list = new ContractList(this.contr_service);
		contr_list.setCustomer(cust);

		// new RouteParameters("userID", "123")
		Button add_contr_btn = new Button("Neuen Vertrag anlegen");
		add_contr_btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		add_contr_btn.addClickListener(e -> {
			List<String> list = new ArrayList();
			list.add(this.customer.get().getId().toString());
			Map<String, List<String>> map = new HashMap<String, List<String>>();
			map.put("cust_id", list);
			UI.getCurrent().navigate("manage/contracts/add", new QueryParameters(map));
		});
		return new VerticalLayout(add_contr_btn, contr_list);
	}

	private VerticalLayout setOfferTabContent(Customer cust) {
		offer_list = new OfferList(offer_service);
		offer_list.setCustomer(cust);
		return new VerticalLayout(new H3("Verfügbare Angebote"), offer_list);
	}

	private TwoSideToolbar setToolbar(Customer cust) {
		save_btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		cancel_btn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		delete_btn.addThemeVariants(ButtonVariant.LUMO_ERROR);
		set_supervise_mode.addThemeVariants(ButtonVariant.MATERIAL_OUTLINED);
		button_ctl.setDefaultVerticalComponentAlignment(Alignment.END);
		button_ctl.setSizeUndefined();

		setEditMode(false);

		return new TwoSideToolbar("Kunde " + cust.getCustnum(), button_ctl);
	}

	private void setEditMode(boolean activate) {
		this.cancel_btn.setVisible(activate);
		this.save_btn.setVisible(activate);

		this.delete_btn.setVisible(!activate);
		this.edit_btn.setVisible(!activate);
		this.set_supervise_mode.setVisible(!activate);
	}

	private void onCustDeleteClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		customer.ifPresent(c -> {
			Dialog d = new Dialog();
			d.setCloseOnEsc(true);
			d.add(new Label("Kunde wirklich löschen? Dies wird alle Verträge und Addressen mitlöschen."));
			d.add(new HorizontalLayout(new Button("Ja, wirklich", ev -> {
				d.close();
				cust_service.removeCustomer(c);
				new SuccessNotification("Kunde erfolgreich gelöscht!").open();
				if (UI.getCurrent() != null) {
					UI.getCurrent().navigate("manage/customers/");
				}
			}), new Button("Oops, lieber doch nicht", ev -> d.close())));
			d.open();
		});
	}

	private void setCustomer(Customer cust) {
		this.customer = Optional.of(cust);
		if (customer.isPresent()) {
			this.offer_list.setCustomer(this.customer.get());
			this.cust_form.setCustomer(this.customer.get());
			this.contr_list.setCustomer(this.customer.get());
		}
	}

	private void onCustEditClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		this.tabs.setSelectedTab(this.tab_detail);
		this.cust_form.setReadOnly(false);
		setEditMode(true);
	}
	
	private void onSuperviseClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		this.tabs.setSelectedTab(this.tab_detail);
		this.cust_form.setSuperviseMode(true);
		setEditMode(true);
	}

	private void onCustSaveClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		Optional<Customer> cust_valid = cust_form.saveCustomer();
		if (cust_valid.isPresent()) {
			this.setCustomer(cust_valid.get());
			this.cust_form.setReadOnly(true);
			setEditMode(false);
			this.cust_form.setSuperviseMode(false);
		} 
	}

	private void onCustCancelClick(com.vaadin.flow.component.ClickEvent<Button> event) {
		this.cust_form.refresh();
		this.cust_form.setReadOnly(true);
		this.cust_form.setSuperviseMode(false);
		setEditMode(false);
	}
	

}
