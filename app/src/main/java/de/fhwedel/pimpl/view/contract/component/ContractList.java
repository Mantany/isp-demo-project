package de.fhwedel.pimpl.view.contract.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.ItemDoubleClickEvent;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.service.ContractService;

/**
 * The UI Element to show the Contract List.
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class ContractList extends Composite<Component> {
	Grid<Contract> grid = new Grid<>(Contract.class);
	TextField search_text = new TextField();
	Button search_btn = new Button();
	HorizontalLayout search_bar = new HorizontalLayout();
	ContractService contr_service;
	Customer filter_by_customer = null;
	
	VerticalLayout view = new VerticalLayout();

	public ContractList(ContractService contr_service) {
		this.contr_service = contr_service;
		this.view.setSizeFull();
		configureGrid();
		configureSearchBar();
		this.view.add(search_bar);
		this.view.add(grid);
		updateList();
	}
	
	public void setCustomer(Customer filter) {
		this.filter_by_customer = filter;
		updateList();
	}
	
	@Override
	protected Component initContent() {
		return view;
	}
	

	private void configureSearchBar() {
		search_text.setPlaceholder("Nach Vertragsnummer suchen");

		search_text.setClearButtonVisible(true);
		search_text.setValueChangeMode(ValueChangeMode.LAZY);
		search_text.setPrefixComponent(VaadinIcon.SEARCH.create());
		
		search_btn.setText("Suchen");
		search_btn.addClickListener(e -> updateList());
		
		search_bar.add(search_text, search_btn);
	}

	private void configureGrid() {
		// Set up the grid:
		grid.setSizeFull();

		grid.removeAllColumns();
		grid.addColumn(Contract::getContractNumber).setHeader("Vertragsnummer").setSortable(true);
		grid.addColumn(Contract::getEntryDate).setHeader("Erstellungsdatum").setSortable(true);
		grid.addColumn(new ComponentRenderer<>(contract -> {
		   return new ContractStatusLabel(contract.getStatus());
		})).setHeader("Status");
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setHeight("300px");
		// grid.addSelectionListener(this::onContrSelect);
		grid.addItemDoubleClickListener(this::onContrNavigate);
		grid.getColumns().forEach(col -> col.setAutoWidth(true));

	}
	
	private void onContrNavigate(ItemDoubleClickEvent<Contract> event) {
		if(UI.getCurrent() != null) {
			UI.getCurrent().navigate("manage/contract/" + event.getItem().getId());
		}
	}
	
	public void updateList() {
		// grid.setItems(contracts);
		if(this.filter_by_customer == null) {
			// no customer -> search all
			grid.setDataProvider(new ListDataProvider<Contract>(contr_service.findAllContracts(search_text.getValue())));
		} else {
			// given customer -> only search customer contracts
			grid.setDataProvider(new ListDataProvider<Contract>(contr_service.findAllContractsByCustomer(search_text.getValue(), this.filter_by_customer)));
		}
		
	}

}
