package de.fhwedel.pimpl.view.offer.component;

import java.util.Optional;
import java.util.function.Consumer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.model.Offer;
import de.fhwedel.pimpl.data.service.OfferService;
import de.fhwedel.pimpl.view.shared.TwoSideToolbar;
import de.fhwedel.pimpl.view.shared.notification.ErrorNotification;

/**
 * Shows the OfferList & OfferForm to select an offer
 * @author Lennard Trippensee
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class OfferSelectionDetail extends Composite<Component>{
	private Contract contract;
	private Optional<Offer> offer;
	private OfferService offer_service;
	private OfferForm offer_form; 
	private OfferList offer_select_list;
	
	private Button accept_btn = new Button("Auswählen");
	private Button cancel_btn = new Button("Abbrechen");

	Optional<Runnable> onCancelListener = Optional.empty();
	Optional<Consumer<Offer>> onSelectedListener = Optional.empty();
	
	
	VerticalLayout view = new VerticalLayout();
	@Override
	protected Component initContent() {
		return view;
	}
	
	public OfferSelectionDetail(OfferService offer_service) {
		assert contract != null;
		
		
		this.offer_service = offer_service;
		
		this.offer_select_list = new OfferList(offer_service);
		this.offer_form = new OfferForm(true);
		this.offer_select_list.listenToClick(new Consumer<Offer>() {
			@Override
			public void accept(Offer t) {
				offer_form.setOffer(Optional.of(t));
				accept_btn.setEnabled(true);
				offer = Optional.of(t);
			}
		});
		
		this.accept_btn.setEnabled(false);
		this.accept_btn.addClickListener(r -> {onSelected();});
		this.cancel_btn.addClickListener(r -> {onCancel();});
		
		this.accept_btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		this.cancel_btn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		
		TwoSideToolbar bar = new TwoSideToolbar("Wählen sie ihr Angebot", new HorizontalLayout(accept_btn, cancel_btn));
		this.view.add(bar, offer_select_list, offer_form);
	}
	
	public void setSuperviseMode(boolean supervise_mode) {
		this.offer_select_list.setSupervisedMode(supervise_mode);
	}
	
	public void setContract(Contract c) {
		this.contract = c;
		Customer cust = contract.getCust();
		this.offer_select_list.setCustomer(cust);
	}
	
	public void listenToOnSelected(Consumer<Offer> onSelectedListener) {
		this.onSelectedListener = Optional.of(onSelectedListener);
	}
	
	public void listenToOnCancel(Runnable r) {
		this.onCancelListener = Optional.of(r);
	}
	
	private void onCancel() {
		this.onCancelListener.ifPresent(r-> {r.run();});
	}
	
	
	private void onSelected() {
		if(this.offer.isPresent()) {
			this.onSelectedListener.ifPresent(r -> {r.accept(this.offer.get());});
		} else {
			new ErrorNotification("Bitte Angebot wählen!").open();
		}
	}


}
