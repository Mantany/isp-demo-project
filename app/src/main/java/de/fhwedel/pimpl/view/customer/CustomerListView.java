package de.fhwedel.pimpl.view.customer;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.service.CustomerService;
import de.fhwedel.pimpl.view.MainView;
import de.fhwedel.pimpl.view.customer.component.CustomerList;
import de.fhwedel.pimpl.view.shared.TwoSideToolbar;

/**
 * A Simple View to show the List of customers
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@Route(value = "customers", layout = MainView.class)
@RouteAlias(value = "", layout = MainView.class)
@SpringComponent
@UIScope
public class CustomerListView extends VerticalLayout implements BeforeEnterObserver {
	private CustomerService cust_service;
	private CustomerList cl;
	private Button add_cust_btn = new Button("Neuer Kunde");
	
	//Update the list on enter:
	@Override
	public void beforeEnter(BeforeEnterEvent event) {
	cl.updateList();		
	}
	
	public CustomerListView(CustomerService cust_service) {
		this.cust_service = cust_service;
		this.cl = new CustomerList(this.cust_service);
		
		add(setToolbar());
		add(cl);
	}
	
	
	private TwoSideToolbar setToolbar() {
		add_cust_btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		add_cust_btn.addClickListener(e -> {
			UI.getCurrent().navigate("manage/customers/add");
		});
		return new TwoSideToolbar("Kunden - Überblick", add_cust_btn);
	}

	
}
