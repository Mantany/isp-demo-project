package de.fhwedel.pimpl.view.contract.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.dom.Style;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Contract.ContractStatus;

/**
 * Component to display the Contract status
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class ContractStatusLabel extends Composite<Component>{
	private Button button = new Button();
	
	public ContractStatusLabel(ContractStatus status) {
		this.setStyle();
		this.setStatus(status);
	}
	
	public ContractStatusLabel() {
		this.setStyle();
		this.button.setText("undefined");
	}
	
	@Override
	protected Component initContent() {
		return button;
	}
	
	private void setStyle() {
		// TODO as own css class:
		this.button.setEnabled(false);
		Style s = this.button.getStyle();
		s.set("font-size", "9px");
		s.set("height", "20px");
		s.set("border", "2px solid");
		s.set("background-color", "transparent");
		s.set("border-radius", "10px");
		s.set("font-weight", "bold");
	}
	
	public void setStatus(ContractStatus status) {
		this.button.setText(status.toString());
		
		//Change the color,d epending ons status:
		Style s = this.button.getStyle();
		String orange = "#FF9E6A";
		String red = "#E06057";
		String green = "#32945C";
		String gray = "#8A8C89";
		
		String color = "";
		switch (status) {
			case ACTIVE:
				color = green;
				break;
			case ORDER:
				color = orange;
				break;
			case REQUEST:
				color = orange;
				break;
			case NOTAVAIL:
				color = red;
				break;
			case REJECTBONI:
				color = red;
				break;
			default:
				color = gray;
		}
			
		s.set("color", color);
	}
	
	
	
}
