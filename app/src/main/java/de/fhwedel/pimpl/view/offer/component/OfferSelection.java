package de.fhwedel.pimpl.view.offer.component;

import java.util.Optional;
import java.util.function.Consumer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.orderedlayout.FlexLayout.WrapMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Offer;
import de.fhwedel.pimpl.data.service.ContractService;
import de.fhwedel.pimpl.data.service.OfferService;

/**
 * Shows an offer Selection button, else the offer
 * 
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class OfferSelection extends Composite<Component> {
	private Optional<Contract> contract = Optional.empty();

	private boolean supervise_mode = false;
	private OfferService offer_service;
	private Dialog selection_dialog;
	private OfferSelectionDetail offer_selection_list;
	private OfferForm offer_form;
	private ContractService contract_service;

	VerticalLayout view = new VerticalLayout();

	@Override
	protected Component initContent() {
		return view;
	}

	public OfferSelection(OfferService offer_service, ContractService contract_service) {
		this.contract_service = contract_service;
		this.offer_service = offer_service;

		this.offer_selection_list = new OfferSelectionDetail(offer_service);

		this.selection_dialog = new Dialog(this.offer_selection_list);
		this.offer_selection_list.listenToOnCancel(new Runnable() {
			@Override
			public void run() {
				selection_dialog.close();

			}
		});

		this.offer_selection_list.listenToOnSelected(new Consumer<Offer>() {

			@Override
			public void accept(Offer t) {
				if (contract.isPresent()) {
					contract.get().setOff(t);
					contract_service.saveContract(contract.get());
					refresh();
					selection_dialog.close();
				}
			}
		});
	}

	public void setSuperviseMode(boolean supervise_mode) {
		this.supervise_mode = supervise_mode;
		this.offer_selection_list.setSuperviseMode(supervise_mode);
		refresh();
	}

	public void setContract(Contract contract) {
		assert contract != null;
		this.contract = Optional.of(contract);
		this.offer_selection_list.setContract(contract);
		refresh();
	}

	private void refresh() {
		this.view.removeAll();
		if (this.contract.isPresent()) {
			VerticalLayout title = new VerticalLayout( new H3("Angebot"));
			title.getStyle().set("margin", "0em");
			FlexLayout bar = new FlexLayout(title);
			view.add(bar);
			bar.setJustifyContentMode(JustifyContentMode.BETWEEN);
			bar.setWidthFull();
			bar.setAlignItems(Alignment.CENTER);
			bar.setWrapMode(WrapMode.WRAP);
			// Only if offer wasnt choosen or supervise is off, make a offer choice possible
			if (this.supervise_mode || this.contract.get().getOff() == null) {
				Button choose = new Button("Angebot auswählen", e -> {
					this.selection_dialog.open();
				});
				choose.addThemeVariants(ButtonVariant.LUMO_SMALL);
				bar.add(new HorizontalLayout(choose));
			}
			if (this.contract.get().getOff() != null) {
				this.offer_form = new OfferForm(false);
				this.offer_form.setOffer(Optional.of(this.contract.get().getOff()));
				view.add(this.offer_form);
			}
		}
	}
}
