package de.fhwedel.pimpl.view.contract.component;

import java.time.LocalDate;
import java.util.Optional;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Contract.ContractStatus;
import de.fhwedel.pimpl.data.service.ContractService;
import de.fhwedel.pimpl.view.shared.notification.ErrorNotification;
import de.fhwedel.pimpl.view.shared.notification.SuccessNotification;
import de.fhwedel.pimpl.view.shared.notification.WarningNotification;

/**
 * The Contract form to display a Contract. Only UI Elements
 * 
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class ContractForm extends Composite<Component> {
	private ContractService contr_service;
	private boolean supervise_mode = false;
	private Optional<Contract> contract = Optional.empty();
	private boolean show_installation_dates = true;
	private Binder<Contract> binder = new BeanValidationBinder<>(Contract.class);

	// UI Child Elements
	private ComboBox<ContractStatus> status = new ComboBox<Contract.ContractStatus>("Status", ContractStatus.values());
	private TextField contractNumber = new TextField("Vertragsnummer");
	private DatePicker entryDate = new DatePicker("Erfassungsdatum");
	private TextField iban = new TextField("IBAN");
	private TextField bic = new TextField("BIC");
	private Checkbox selfInstallation = new Checkbox("Eingeninstallation", this::onselfInstallationValueChange);
	private TextArea comment = new TextArea("Kommentar");

	private DatePicker activatedOnDate = new DatePicker("Aktiviert am");

	private DatePicker installedOnDate = new DatePicker("Installiert am");
	private DatePicker installationDate = new DatePicker("Installationstermin");
	private FormLayout install_form = new FormLayout(installationDate, installedOnDate);
	private H4 install_title = new H4("Installation");

	private DatePicker startDate = new DatePicker("Vertragsbeginn");

	private FormLayout detail_form;

	private VerticalLayout view = new VerticalLayout();

	@Override
	protected Component initContent() {
		return view;
	}

	/**
	 * 
	 * @param contr_service           the contract service to communicate with
	 *                                business logic
	 * @param show_installation_dates true/false shows the installation dates
	 * @param show_status             true/false whether the status should be
	 *                                visible
	 * @param show_activated_on_date  true/false whether the activated on field
	 *                                should be visible
	 */
	public ContractForm(ContractService contr_service, boolean show_installation_dates, boolean show_status,
			boolean show_activated_on_date) {
		this.contr_service = contr_service;
		this.show_installation_dates = show_installation_dates;

		this.detail_form = getDetailsForm();
		this.status.setVisible(show_status);
		this.activatedOnDate.setVisible(show_activated_on_date);

		binder.bindInstanceFields(this);
		view.add(this.detail_form);
		view.add(this.install_title);
		view.add(this.install_form);

		// Set up the visibility of the install date details
		this.showInstallation();

		this.setBusinessValidation(true);
		this.setReadOnly(true);
	}

	/**
	 * On checkbox click, check the dates
	 * 
	 * @param event
	 */
	private void onselfInstallationValueChange(ComponentValueChangeEvent<Checkbox, Boolean> event) {
		setMinStartDateValue(event.getValue());
		if (this.show_installation_dates) {
			setInstallationVisibility(!event.getValue());
		}
	}

	private void setMinStartDateValue(boolean self_install) {
		if (!this.supervise_mode) {
			binder.validate();
			this.startDate.setMin(this.contr_service.getMinStartDate(self_install));
		}
	}

	public void setReadOnly(boolean writeable) {
		this.status.setReadOnly(writeable);
		this.contractNumber.setReadOnly(writeable);
		this.entryDate.setReadOnly(writeable);
		this.iban.setReadOnly(writeable);
		this.bic.setReadOnly(writeable);
		this.startDate.setReadOnly(writeable);
		this.selfInstallation.setReadOnly(writeable);
		this.comment.setReadOnly(writeable);
		this.activatedOnDate.setReadOnly(writeable);
		this.installedOnDate.setReadOnly(writeable);
		this.installationDate.setReadOnly(writeable);
	}

	/**
	 * Turns the Business Validation on or off.
	 * 
	 * @param validation true = validation on, off = validation off
	 */
	private void setBusinessValidation(boolean validation) {
		if (validation) {
			// Start validation:
			binder.forField(startDate).withValidator(
					// if null -> return false
					date -> date != null
							? (date).compareTo(contr_service.getMinStartDate(selfInstallation.getValue())) >= 0
							: false,
					"Bitte beachten sie das Mindestdatum.")
					.bind(Contract::getStartDate, Contract::setStartDate);
			setMinStartDateValue(selfInstallation.getValue());

			this.installationDate.setMin(LocalDate.now());
			this.installedOnDate.setMin(LocalDate.now());

		} else {
			// remove all validators
			this.startDate.setMin(null);
			this.installationDate.setMin(null);
			this.installedOnDate.setMin(null);
			binder.removeBinding(startDate);
			binder.bindInstanceFields(this);

		}

	}

	public void setSuperviseMode(boolean writeable) {
		this.supervise_mode = writeable;
		this.setReadOnly(!writeable);
		this.status.setReadOnly(!writeable);
		this.status.setVisible(writeable);
		this.setBusinessValidation(!writeable);
	}

	/**
	 * Sets the Installation section based on contract if contract.self installation
	 * == true, set the visibility of installation dates to false
	 * 
	 * toggled on or of by the show_installation_dates variables
	 */
	private void showInstallation() {
		if (this.show_installation_dates) {
			this.contract.ifPresent(c -> {
				setInstallationVisibility(!c.getSelfInstallation());
			});
		} else {
			setInstallationVisibility(false);
		}

	}

	private void setInstallationVisibility(boolean visible) {
		this.install_form.setVisible(visible);
		this.install_title.setVisible(visible);
	}

	private FormLayout getDetailsForm() {
		FormLayout form = new FormLayout();
		form.add(status);
		form.add(contractNumber, entryDate, iban, bic, startDate, selfInstallation, comment);
		form.setColspan(comment, 3);
		form.add(activatedOnDate);
		form.setColspan(activatedOnDate, 3);
		return form;

	}

	public void setInstallationFieldsReadOnly(boolean enabled) {
		this.installationDate.setReadOnly(enabled);
		this.installedOnDate.setReadOnly(enabled);
	}

	/**
	 * Used to refresh the Contract Form.
	 */
	public void refresh() {
		binder.readBean(contract.orElse(null));
		showInstallation();
	}

	/**
	 * Set the filled Contract fields (fields != null) to readonly
	 * 
	 * @param readonly true:set to readonly; false: set to read/write
	 */
	public void setFilledFieldsReadOnly(boolean readonly) {
		if (contract.isPresent()) {
			if (contract.get().getStatus() != null) {
				this.status.setReadOnly(readonly);
			}
			if (contract.get().getContractNumber() != null) {
				this.contractNumber.setReadOnly(readonly);
			}
			if (contract.get().getEntryDate() != null) {
				this.entryDate.setReadOnly(readonly);
			}
			if (contract.get().getStartDate() != null) {
				this.startDate.setReadOnly(readonly);
			}
			if (contract.get().getComment() != null) {
				this.comment.setReadOnly(readonly);
			}
			if (contract.get().getIban() != null) {
				this.iban.setReadOnly(readonly);
			}
			if (contract.get().getBic() != null) {
				this.bic.setReadOnly(readonly);
			}
			if (contract.get().getSelfInstallation() != null) {
				this.selfInstallation.setReadOnly(readonly);
			}
			if (contract.get().getInstallationDate() != null) {
				this.installationDate.setReadOnly(readonly);
			}
			if (contract.get().getInstalledOnDate() != null) {
				this.installedOnDate.setReadOnly(readonly);
			}
			if (contract.get().getActivatedOnDate() != null) {
				this.activatedOnDate.setReadOnly(readonly);
			}
		}
	}

	/**
	 * Method to set the Contract and refreshes the current Form
	 * 
	 * @param contract
	 * @param setFilledFielsReadOnly
	 */
	public void setContract(Contract contr) {
		this.contract = Optional.of(contr);
		refresh();
	}

	/**
	 * Validate the input and saves the contract, else returns empty Optional.
	 * 
	 * @return valid contract, else empty optional
	 */
	public Optional<Contract> saveContract() { 
		if (contract.isPresent() && binder.writeBeanIfValid(contract.get())) {
			// Business Rule -> Vorgaben Terminplanung Installation
			if(!this.supervise_mode && this.contr_service.startDateIsOverlappingWithInstallDate(contract.get())) {
				this.contract.get().setStartDate(contract.get().getInstallationDate());
				new WarningNotification("Achtung: Vertragsbeginn wurde verändert!").open();
			}
			Optional<Contract> contr_business_valid = contr_service.saveContract(this.contract.get());
			if (contr_business_valid.isPresent()) {
				this.contract = contr_business_valid;
				this.setContract(contr_business_valid.get());
				new SuccessNotification("Änderungen gespeichert!").open();
				return contract;
			} else {
				new ErrorNotification("Es ist ein Fehler beim erstellen des Vertrages aufgetreten.").open();
			}
		} else {
			new ErrorNotification("Bitte alle Felder korrekt ausfüllen").open();
		}
		return Optional.empty();
	}

	/**
	 * Returnes the current Contract
	 * 
	 * @return could be empty
	 */
	public Optional<Contract> getContract() {
		return this.contract;
	}

}
