package de.fhwedel.pimpl.view;

import java.util.HashMap;
import java.util.Map;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.Tabs.Orientation;
import com.vaadin.flow.component.tabs.Tabs.SelectedChangeEvent;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.spring.annotation.UIScope;

import de.fhwedel.pimpl.view.contract.ContractListView;
import de.fhwedel.pimpl.view.customer.CustomerListView;

/**
 * The Main View of the application, acts as a parent Layout to other views.
 * @author Lennard Trippensee(imca103936)
 *
 */
@SuppressWarnings("serial")
@RoutePrefix("manage")
@UIScope
public class MainView extends Composite<Component>implements RouterLayout {

	private Tab t1 = new Tab(new RouterLink("Kunden", CustomerListView.class));
	private Tab t2 = new Tab(new RouterLink("Verträge", ContractListView.class));
	private Tabs ts = new Tabs(false, t1, t2);
	private AppLayout view = new AppLayout();

	private Map<Tab, Component> tabs = new HashMap<>();

	public MainView(CustomerListView cv, ContractListView clv) {
		
		view.setPrimarySection(AppLayout.Section.DRAWER);
        Label heading = new Label("ISP Management Portal");
        view.addToNavbar(new DrawerToggle(), heading);
		
		tabs.put(t1, cv);
		tabs.put(t2, clv);
		ts.setOrientation(Orientation.VERTICAL);
		ts.addSelectedChangeListener(this::tabChanged);
		view.addToDrawer(ts);
		view.setDrawerOpened(false);
	}

	@Override
	protected Component initContent() {
		return view;
	}

	private void tabChanged(SelectedChangeEvent event) {
		view.setContent(tabs.get(event.getSelectedTab()));
	}

}
