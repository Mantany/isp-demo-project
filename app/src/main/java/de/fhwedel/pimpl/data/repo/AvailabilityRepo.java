package de.fhwedel.pimpl.data.repo;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import de.fhwedel.pimpl.data.model.Availability;

public interface AvailabilityRepo extends JpaRepositoryImplementation<Availability, Integer> {
}