package de.fhwedel.pimpl.data.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import de.fhwedel.pimpl.data.model.Contract;

public interface ContractRepo extends JpaRepository<Contract, Integer> {
	
	@Query("select c from Contract c " +
	        "where lower(c.contractNumber) like lower(concat('%', :search_string, '%')) ")
//	        + "or lower(c.lastName) like lower(concat('%', :searchTerm, '%'))")
	List<Contract> searchAll(@Param("search_string") String search_string);
	
	@Query("select c from Contract c where lower(c.cust.id) like :cust_id")
	List<Contract> getAllByCustomer( @Param("cust_id")String cust_id);
	
	@Query("select c from Contract c where lower(c.cust.id) like :cust_id "
			+ "and lower(c.contractNumber) like lower(concat('%', :search_string, '%'))")
	List<Contract> searchByCustomer(@Param("search_string") String search_string, @Param("cust_id")String cust_id);
}
