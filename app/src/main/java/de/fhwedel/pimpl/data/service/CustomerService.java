package de.fhwedel.pimpl.data.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import org.springframework.stereotype.Service;

import de.fhwedel.pimpl.data.model.Address;
import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.repo.AddressRepo;
import de.fhwedel.pimpl.data.repo.ContractRepo;
import de.fhwedel.pimpl.data.repo.CustomerRepo;

/**
 * The Service to provide the Business Logic of the Customer Featureset. Also,
 * provides the data operations to throw nessesary exceptions
 * 
 * @author Lennard Trippensee(imca103936)
 * 
 *         NOTE: Architektur-Entscheidung: Am besten kommuniziert die UI
 *         ausschließlich mit den Services -> Bessere Wartbeirkeit durch
 *         hinzukommen von Business-Rules -> z.B. Fehler/Validierung von
 *         unzulässigen werten
 */
@Service
public class CustomerService {
	private final CustomerRepo cust_repo;
	private final AddressRepo addr_repo;
	private final ContractRepo contr_repo;
	private final ContractService contr_service;

	public CustomerService(CustomerRepo cust_repo, ContractRepo contr_repo, AddressRepo addr_repo,
			ContractService contr_service) {
		this.contr_service = contr_service;
		this.cust_repo = cust_repo;
		this.addr_repo = addr_repo;
		this.contr_repo = contr_repo;
	}
	
	/**
	 * Used to generate a CustomerInstance.
	 * Business-Prozess: Kunde Anlegen -> Erhebungsumfang
	 * @return 
	 */
	public Customer generateCustomer() {
		Customer result = new Customer();
		result.setCustnum("K" + (cust_repo.count() + 1));
		return result;
	}

	/**
	 * Finds the Customers by surname, lastname and custno. if no search string
	 * given, return all customers
	 * 
	 * @param search_string
	 * @return list of customers
	 */
	public List<Customer> findAllCustomer(String search_string) {
		if (search_string == null || search_string.isEmpty()) {
			return cust_repo.findAll();
		} else {
			return cust_repo.searchAll(search_string);
		}
	}

	public Optional<Customer> findCustomerById(Integer id) {
		return cust_repo.findById(id);
	}

	/**
	 * Saves the address into the database and binds the customer to it. (if not
	 * already bind)
	 * 
	 * @param a the Address to be created
	 * @param c the customer to bind the address to
	 * @return The address, if successful, null if not
	 */
	public Optional<Address> saveAddress(Address a, Customer c) {
		Optional<Address> addr = Optional.of(addr_repo.save(a));
		if (a.getCust() == null) {
			a.setCust(c);
			c.getAddresses().add(a);
			c = cust_repo.save(c);
		}
		return addr;
	}

	/**
	 * Removes the given Address from the database and from the customer.
	 * 
	 * @param a the Adress to be removed
	 * @param c the customer to remove thea ddress from
	 * @return
	 */
	public void deleteAddress(Address a) {
		Customer c = a.getCust();
		Set<Address> newaddr = c.getAddresses();
		newaddr.remove(a);
		c.setAddresses(newaddr);
		a.setCust(null);
		addr_repo.delete(a);
		cust_repo.save(c);
	}

	public void removeCustomer(Customer c) {
		Set<Contract> contracts = c.getContracts();
		for (Contract contract : contracts) {
			contr_service.removeContract(contract);
		}
		cust_repo.delete(c);
	}

	/**
	 * Saves the Customer and checks the Business Rules
	 * 
	 * @param c
	 * @return
	 */
	public Optional<Customer> saveCustomer(Customer c) {
		return Optional.of(cust_repo.save(c));
	}

	/**
	 * gets and saves a new credit index and saves it to the customer object c
	 * 
	 * @param c the customer to query for
	 * @return the changed customer object else empty
	 */
	public Optional<Customer> saveCreditIndex(Customer c) {
		c.setCreditIndex(getCreditIndex(c));
		c.setCreditUpdatedDate(LocalDate.now());
		return Optional.of(cust_repo.save(c));
	}

	/**
	 * Fakes a query to a credit-institute to return an Credit index
	 * 
	 * @param c the Customer object
	 * @return null if something goes wrong, else number between 100-600
	 */
	private Integer getCreditIndex(Customer c) {
		
		if(c.getCreditUpdatedDate() != null && !c.getCreditUpdatedDate().isAfter(LocalDate.now())) {
			return c.getCreditIndex();
		} else {
			return new Random().nextInt((600 - 100) + 1) + 100;
		}
	}

	// the Business checks

	/**
	 * Business Check: "Einstufung Bonität"
	 * 100 = best 
	 * 600 = worst
	 * @param c the Customer
	 * @return
	 */
	public boolean checkCreditScore(Customer c) {
		return c.getCreditIndex() <= 300;
	}

	/**
	 * Business Check: "Ermittlung Bonität"
	 * 
	 * @param c
	 * @return true => credit date valid, false => credit date not valid
	 */
	public boolean checkCreditDate(Customer c) {
		return c.getCreditUpdatedDate() != null && !c.getCreditUpdatedDate().isBefore(LocalDate.now().minusYears(1));
	}

}
