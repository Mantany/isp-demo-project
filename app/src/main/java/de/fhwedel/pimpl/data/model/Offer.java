package de.fhwedel.pimpl.data.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class Offer {
	private Integer id;
	private String description;
	private Integer minDuration;
	private Integer bandwidthDownlink;
	private Integer bandwidthUplink;
	private Integer monthPrice;
	private Set<Availability> avail; 
	private Set<Contract> contr;
	
	public Offer() {
	}
	
	
	public Offer(String description, Integer minDuration, Integer bandwidthDownlink, Integer bandwidthUplink,
			Integer monthPrice) {
		super();
		this.description = description;
		this.minDuration = minDuration;
		this.bandwidthDownlink = bandwidthDownlink;
		this.bandwidthUplink = bandwidthUplink;
		this.monthPrice = monthPrice;
	}

	@OneToMany(mappedBy = "off", orphanRemoval = true, cascade = CascadeType.ALL)
	public Set<Contract> getContr() {
		return contr;
	}

	public void setContr(Set<Contract> contr) {
		this.contr = contr;
	}

	@OneToMany(mappedBy = "offer", orphanRemoval = true, cascade = CascadeType.ALL)
	public Set<Availability> getAvail() {
		return avail;
	}
	public void setAvail(Set<Availability> avail) {
		this.avail = avail;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "offer_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@NotNull(message = "Pflichtangabe")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@NotNull(message = "Pflichtangabe")
	@Min(0)
	@Max(24) 
	public Integer getMinDuration() {
		return minDuration;
	}
	public void setMinDuration(Integer minDuration) {
		this.minDuration = minDuration;
	}
	@NotNull(message = "Pflichtangabe")
	@Min(0)
	public Integer getBandwidthDownlink() {
		return bandwidthDownlink;
	}
	public void setBandwidthDownlink(Integer bandwidthDownlink) {
		this.bandwidthDownlink = bandwidthDownlink;
	}
	@NotNull(message = "Pflichtangabe")
	@Min(0)
	public Integer getBandwidthUplink() {
		return bandwidthUplink;
	}
	public void setBandwidthUplink(Integer bandwidthUplink) {
		this.bandwidthUplink = bandwidthUplink;
	}
	@NotNull(message = "Pflichtangabe")
	@Min(0)
	public Integer getMonthPrice() {
		return monthPrice;
	}
	public void setMonthPrice(Integer monthPrice) {
		this.monthPrice = monthPrice;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bandwidthDownlink == null) ? 0 : bandwidthDownlink.hashCode());
		result = prime * result + ((bandwidthUplink == null) ? 0 : bandwidthUplink.hashCode());
		result = prime * result + ((monthPrice == null) ? 0 : monthPrice.hashCode());
		result = prime * result + ((avail == null) ? 0 : avail.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((minDuration == null) ? 0 : minDuration.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Offer other = (Offer) obj;
		if (bandwidthDownlink == null) {
			if (other.bandwidthDownlink != null)
				return false;
		} else if (!bandwidthDownlink.equals(other.bandwidthDownlink))
			return false;
		if (bandwidthUplink == null) {
			if (other.bandwidthUplink != null)
				return false;
		} else if (!bandwidthUplink.equals(other.bandwidthUplink))
			return false;
		if (monthPrice == null) {
			if (other.monthPrice != null)
				return false;
		} else if (!monthPrice.equals(other.monthPrice))
			return false;
		if (avail == null) {
			if (other.avail != null)
				return false;
		} else if (!avail.equals(other.avail))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (minDuration == null) {
			if (other.minDuration != null)
				return false;
		} else if (!minDuration.equals(other.minDuration))
			return false;
		return true;
	}
}