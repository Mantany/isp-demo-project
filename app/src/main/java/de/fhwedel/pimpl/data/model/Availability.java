package de.fhwedel.pimpl.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Availability {
	private Integer id;
	private String startPlz;
	private String endPlz;
	private Offer offer;
	
	public Availability() {
	}
	
	public Availability(String startPlz, String endPlz, Offer offer) {
		super();
		this.startPlz = startPlz;
		this.endPlz = endPlz;
		this.offer = offer;
	}

	@ManyToOne
	public Offer getOffer() {
		return offer;
	}
	
	public void setOffer(Offer offer) {
		this.offer = offer;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "avail_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@NotNull
	@Size(min = 1, message = "Mindestens ein Zeichen Länge")
	//International können beliebige zeichen vorkommen als PLZ.. 
	public String getStartPlz() {
		return startPlz;
	}
	public void setStartPlz(String startPlz) {
		this.startPlz = startPlz;
	}
	@NotNull
	@Size(min = 1, message = "Mindestens ein Zeichen Länge")
	//International können beliebige zeichen vorkommen als PLZ.. 
	public String getEndPlz() {
		return endPlz;
	}
	public void setEndPlz(String endPlz) {
		this.endPlz = endPlz;
	}

	@Override
	public String toString() {
		return "Availability [id=" + id + ", startPlz=" + startPlz + ", endPlz=" + endPlz + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endPlz == null) ? 0 : endPlz.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((startPlz == null) ? 0 : startPlz.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Availability other = (Availability) obj;
		if (endPlz == null) {
			if (other.endPlz != null)
				return false;
		} else if (!endPlz.equals(other.endPlz))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (startPlz == null) {
			if (other.startPlz != null)
				return false;
		} else if (!startPlz.equals(other.startPlz))
			return false;
		return true;
	}
	
}