package de.fhwedel.pimpl.data.repo;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import de.fhwedel.pimpl.data.model.Offer;

public interface OfferRepo extends JpaRepositoryImplementation<Offer, Integer> {
}
