package de.fhwedel.pimpl.data.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Customer {
	//TODO Bonität und bonitätsdatum setzten
	public enum Salutation {
		FEMALE, MALE, DIVERSE
	};

	private Integer id;

	private String custnum;

	private Salutation salut;

	private String surname;

	private String prename;

	private Set<Address> addresses;
	
	private Set<Contract> contracts;
	
	private Integer creditIndex;
	
	private LocalDate creditUpdatedDate;

	public Customer() {
		this.addresses = new HashSet<Address>();
		this.contracts = new HashSet<Contract>();
	}

	public Customer(Salutation salut, String custnum, String surname, String prename) {
		this();
		this.salut = salut;
		this.custnum = custnum;
		this.surname = surname;
		this.prename = prename;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "cust_id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull(message = "Pflichtangabe")
	public Salutation getSalutation() {
		return salut;
	}

	public void setSalutation(Salutation salut) {
		this.salut = salut;
	}

	@NotNull(message = "Pflichtangabe")
	public String getCustnum() {
		return custnum;
	}

	public void setCustnum(String custnum) {
		this.custnum = custnum;
	}

	@Size(min = 2, message = "Mindestens zwei Zeichen Länge")
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Size(min = 2, message = "Mindestens zwei Zeichen Länge")
	public String getPrename() {
		return prename;
	}

	public void setPrename(String prename) {
		this.prename = prename;
	}

	@NotNull
	@OneToMany(mappedBy = "cust", orphanRemoval = true, cascade = CascadeType.ALL)
	public Set<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	
	@OneToMany(mappedBy = "cust", orphanRemoval = true, cascade = CascadeType.ALL)
	public Set<Contract> getContracts() {
		return contracts;
	}

	public void setContracts(Set<Contract> contracts) {
		this.contracts = contracts;
	}

	@Min(value = 100)
	@Max(value = 600)
	public Integer getCreditIndex() {
		return creditIndex;
	}

	public void setCreditIndex(Integer creditIndex) {
		this.creditIndex = creditIndex;
	}

	public LocalDate getCreditUpdatedDate() {
		return creditUpdatedDate;
	}

	public void setCreditUpdatedDate(LocalDate creditUpdatedDate) {
		this.creditUpdatedDate = creditUpdatedDate;
	}

	@Override
	public String toString() {
		return "Customer [custid=" + id + ", custnum=" + custnum + ", salutation=" + salut + ", surname=" + surname
				+ ", prename=" + prename + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addresses == null) ? 0 : addresses.hashCode());
		result = prime * result + ((salut == null) ? 0 : salut.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((custnum == null) ? 0 : custnum.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		result = prime * result + ((prename == null) ? 0 : prename.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (addresses == null) {
			if (other.addresses != null)
				return false;
		} else if (!addresses.equals(other.addresses))
			return false;
		if (salut != other.salut)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (custnum == null) {
			if (other.custnum != null)
				return false;
		} else if (!custnum.equals(other.custnum))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		if (prename == null) {
			if (other.prename != null)
				return false;
		} else if (!prename.equals(other.prename))
			return false;
		return true;
	}

}