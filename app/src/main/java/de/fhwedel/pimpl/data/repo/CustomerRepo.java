package de.fhwedel.pimpl.data.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Customer;

public interface CustomerRepo extends JpaRepository<Customer, Integer> {
	@Query("select c from Customer c " +
	        "where lower(c.custnum) like lower(concat('%', :search_string, '%')) " +
	        "or lower(c.surname) like lower(concat('%', :search_string, '%')) " +
	        "or lower(c.prename) like lower(concat('%', :search_string, '%')) " )
	List<Customer> searchAll(@Param("search_string") String search_string);
}
