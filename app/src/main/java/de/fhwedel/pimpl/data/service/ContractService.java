package de.fhwedel.pimpl.data.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import de.fhwedel.pimpl.data.model.Contract;
import de.fhwedel.pimpl.data.model.Contract.ContractStatus;
import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.repo.ContractRepo;
import de.fhwedel.pimpl.data.repo.CustomerRepo;

/**
 * This class contains the business logic of Contract related operations. Also
 * is used to check the business rules are met before writing + reading of data
 * 
 * @author Lennard Trippensee(imca103936)
 *
 */

@Service
public class ContractService {
	private final ContractRepo contr_repo;
	private final CustomerRepo cust_repo;

	public ContractService(ContractRepo contr_repo, CustomerRepo cust_repo) {
		this.cust_repo = cust_repo;
		this.contr_repo = contr_repo;
	}

	public List<Contract> findAllContracts(String search_string) {
		if (search_string == null || search_string.isEmpty()) {
			return contr_repo.findAll();
		} else {
			return contr_repo.searchAll(search_string);
		}
	}

	public List<Contract> findAllContractsByCustomer(String search_string, Customer c) {
		if (search_string == null || search_string.isEmpty()) {
			return contr_repo.getAllByCustomer(c.getId().toString());
		} else {
			return contr_repo.searchByCustomer(search_string, c.getId().toString());
		}

	}

	/**
	 * This Method is used to generate a basic contract 
	 * Business-Prozess: Vertrag Anlegen ->Erhebungsumfang
	 * 
	 * @return
	 */
	public Contract generateContract() {
		Contract result = new Contract();
		result.setEntryDate(LocalDate.now());
		result.setStatus(ContractStatus.REQUEST);
		result.setContractNumber("V" + LocalDate.now().getYear() + (contr_repo.count() + 1));
		result.setConfirmationSend(false);
		return result;
	}

	/**
	 * Gets the MinimalStartDate of the Contract based of bunises rules. PROZESS:
	 * Vertrag Anlegen -> rückwirkender Vertragsbeginn,
	 * 
	 * @param c
	 * @return
	 */
	public LocalDate getMinStartDate(boolean self_install) {
		LocalDate result = LocalDate.now();
		// setzten des puffers für außendienst:
		if (!self_install) {
			result = result.plusWeeks(2);
		}
		return result;
	}

	public void removeContract(Contract c) {
		if (c != null) {
			contr_repo.delete(c);
		}
	}

	public Optional<Contract> saveContract(Contract c) {
		Contract result = contr_repo.save(c);
		result.getCust().getContracts().add(c);
		cust_repo.save(result.getCust());
		return Optional.of(contr_repo.save(c));
	}

	public Optional<Contract> findContractById(Integer id) {
		return contr_repo.findById(id);
	}

	/**
	 * Gets the possible Contract Status that are currently allowed
	 * 
	 * @param contract
	 * @param superuser
	 * @return
	 */
	public List<ContractStatus> getAllowedContractStatus(Contract contract) {

		List<ContractStatus> result = new ArrayList<ContractStatus>();
		switch (contract.getStatus()) {
		case REQUEST:
			result.add(ContractStatus.REQUEST);
			result.add(ContractStatus.NOTAVAIL);
			result.add(ContractStatus.REJECTBONI);
			result.add(ContractStatus.ORDER);
			break;
		case ORDER:
			result.add(ContractStatus.ORDER);
			result.add(ContractStatus.ACTIVE);
			break;
		case ACTIVE:
			result.add(ContractStatus.ACTIVE);
			result.add(ContractStatus.INACTIVE);
			result.add(ContractStatus.CANCEL);
			break;
		case INACTIVE:
			result.add(ContractStatus.ACTIVE);
			result.add(ContractStatus.INACTIVE);
			result.add(ContractStatus.CANCEL);
			break;
		default:
			result.add(contract.getStatus());
		}

		return result;
	}

	public void setDatum(Contract c) {
		// prüfung ob gut is
		this.contr_repo.save(c);
	}

	// The Business checks:

	/**
	 * Returns true, if contract has offer Business-check AngebotAuswählen ->
	 * Angebot zuordnen
	 * 
	 * @param c the contract to check
	 * @return valid/not valid
	 */
	public boolean checkOffer(Contract c) {
		return c.getOff() != null;
	}

	/**
	 * Business-check -> Installation planen oder Eigeninstallation
	 * 
	 * @param c the contract to check
	 * @return valid/not valid
	 */
	public boolean checkInstallDates(Contract c) {
		return c.getSelfInstallation() || (c.getInstallationDate() != null && c.getInstalledOnDate() != null);
	}

	/**
	 * Checks wether the Activation Time is >= now Business-check -> >= Vertrag
	 * "Vertragsbeginn"
	 * 
	 * @param c the contract to check
	 * @return valid/not valid
	 */
	public boolean checkStartDateForActivation(Contract c) {
		return !c.getStartDate().isAfter(LocalDate.now());
	}

	/**
	 * Business-check -> Vorgaben Terminplanung Installation
	 * 
	 * @param c the contract to check
	 * @return valid/not valid
	 */
	public boolean startDateIsOverlappingWithInstallDate(Contract c) {
		if (c.getInstallationDate() != null && c.getStartDate() != null) {
			return (!c.getSelfInstallation()) && c.getStartDate().compareTo(c.getInstallationDate()) < 0;
		}
		return false;
	}
	
	/**
	 * Business-check -> Anchluss aktivieren -> erfassungsumfang
	 * @param c the contract to check
	 * @return valid/not valid
	 */
	public boolean startDateIsOverlappingWithInstalledOnDate(Contract c) {
		if (c.getInstalledOnDate() != null && c.getStartDate() != null) {
			return (!c.getSelfInstallation()) && c.getStartDate().compareTo(c.getInstalledOnDate()) < 0;
		}
		return false;
	}
	
	
	
	/**
	 * Business-check -> Buchungsbestätigung muss versendet worden sein, damit der Vertrag auf aktiv gestellt werden kann.
	 * @param c the contract to check
	 * @return valid/not valid
	 */
	public boolean checkConfirmationSent(Contract c) {
		return c.getConfirmationSend();
	}
}
