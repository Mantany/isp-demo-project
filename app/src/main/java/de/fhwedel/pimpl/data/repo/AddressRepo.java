package de.fhwedel.pimpl.data.repo;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import de.fhwedel.pimpl.data.model.Address;

public interface AddressRepo extends JpaRepositoryImplementation<Address, Integer> {
}
