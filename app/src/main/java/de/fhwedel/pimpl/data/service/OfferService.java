package de.fhwedel.pimpl.data.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import de.fhwedel.pimpl.data.model.Address;
import de.fhwedel.pimpl.data.model.Availability;
import de.fhwedel.pimpl.data.model.Customer;
import de.fhwedel.pimpl.data.model.Offer;
import de.fhwedel.pimpl.data.repo.AvailabilityRepo;
import de.fhwedel.pimpl.data.repo.OfferRepo;

/**
 * This class contains the business logic of Offer related operations. Also
 * is used to check the business rules are met before writing + reading of data
 * @author Lennard Trippensee(imca103936)
 *
 */
@Service
public class OfferService {
	private final OfferRepo offer_repo;
	private final AvailabilityRepo avail_repo;

	public OfferService(OfferRepo offer_repo, AvailabilityRepo avail_repo) {
		this.avail_repo = avail_repo;
		this.offer_repo = offer_repo;
	}

	public List<Offer> findAllOffers() {
		return offer_repo.findAll();
	}

	/**
	 * Finds all Offers that are suitable for the given customer
	 * uses bubblesort to determine the right Offers
	 * @param cust Filter by Customer
	 * @return the List of matching offers
	 */
	public List<Offer> findAllOffersByCustomer(Customer cust) {
		Set<Offer> result = new HashSet<Offer>();
		List<Availability> avails = avail_repo.findAll();
		for (Availability avail : avails) {
			if(isAvailable(avail, cust)) {
				result.add(avail.getOffer());
			}
		}
		return new ArrayList<Offer>(result);
	}

	private boolean isAvailable(Availability a, Customer c) {
		Set<Address> addresses = c.getAddresses();
		boolean result = false;
		for (Address address : addresses) {
			if (isAvailable(a, address) && !result) {
				result = true;
			}
		}
		return result;
	}

	private boolean isAvailable(Availability a, Address addr) {
		try {
			Integer addrPlz = Integer.valueOf(addr.getZip());
			Integer aStartPlz = Integer.valueOf(a.getStartPlz());
			Integer aEndPlz = Integer.valueOf(a.getEndPlz());
			return aStartPlz <= addrPlz && addrPlz <= aEndPlz;
		} catch (NumberFormatException e) {
			// If one Integer cant be convertet to Integer, return false
			return false;
		}

	}

	public void saveOffer(Offer o) {
		offer_repo.save(o);
	}

	public void saveAvailability(Availability a) {
		avail_repo.save(a);
		a.getOffer().getAvail().add(a);
		offer_repo.save(a.getOffer());
	}
	
}
