package de.fhwedel.pimpl.data.model;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class Contract {
	public enum ContractStatus {
		REQUEST, REJECTBONI, NOTAVAIL, ORDER, ACTIVE, CANCEL, INACTIVE
	};
	
	private Integer id;
	private String contractNumber;
	private LocalDate entryDate;
	private ContractStatus status;
	private String comment;
	private LocalDate startDate;
	private String iban;
	private String bic;
	private Boolean selfInstallation; 
	private LocalDate installationDate;
	private LocalDate installedOnDate;
	private LocalDate activatedOnDate;
	private Customer cust;
	private Offer off;
	private Boolean confirmationSend;
	
	
	public Contract(String contractNumber, LocalDate entryDate, ContractStatus status, String comment,
			LocalDate startDate, String iban, String bic, Boolean selfInstallation, LocalDate installationDate,
			LocalDate installedOnDate, LocalDate activatedOnDate) {
		this();
		this.contractNumber = contractNumber;
		this.entryDate = entryDate;
		this.status = status;
		this.comment = comment;
		this.startDate = startDate;
		this.iban = iban;
		this.bic = bic;
		this.selfInstallation = selfInstallation;
		this.installationDate = installationDate;
		this.installedOnDate = installedOnDate;
		this.activatedOnDate = activatedOnDate;
		this.confirmationSend = false;
	}


	public Contract() {
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "contr_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@NotNull(message = "Pflichtangabe")
	@Pattern(regexp = "V([0-9]*)", message = "Bitte im richtigem Format angeben: V<Jahr><Nummer>")
	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	@NotNull(message = "Pflichtangabe")
	public LocalDate getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDate entryDate) {
		this.entryDate = entryDate;
	}
	@NotNull(message = "Pflichtangabe")
	public ContractStatus getStatus() {
		return status;
	}

	public void setStatus(ContractStatus status) {
		this.status = status;
	}

	@NotNull(message = "Pflichtangabe")
	@Size(min = 1, message = "Bitte einen Kommentar angeben!")
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	@NotNull(message = "Pflichtangabe")
	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	@NotNull(message = "Pflichtangabe")
	@Pattern(regexp = "^([A-Z]{2}[ \\-]?[0-9]{2})(?=(?:[ \\-]?[A-Z0-9]){9,30}$)((?:[ \\-]?[A-Z0-9]{3,5}){2,7})([ \\-]?[A-Z0-9]{1,3})?$", message = "Bitte eine gültige IBAN angeben!")
	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}
	
	@NotNull(message = "Pflichtangabe")
	@Size(min = 8, message = "Bitteg eine gültige BIC eingeben!")
	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	@NotNull(message = "Pflichtangabe")
	public Boolean getSelfInstallation() {
		return selfInstallation;
	}

	public void setSelfInstallation(Boolean selfInstallation) {
		this.selfInstallation = selfInstallation;
	}

	public LocalDate getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(LocalDate installationDate) {
		this.installationDate = installationDate;
	}

	public LocalDate getInstalledOnDate() {
		return installedOnDate;
	}

	public void setInstalledOnDate(LocalDate installedOnDate) {
		this.installedOnDate = installedOnDate;
	}
	
	public LocalDate getActivatedOnDate() {
		return activatedOnDate;
	}

	public void setActivatedOnDate(LocalDate activatedOnDate) {
		this.activatedOnDate = activatedOnDate;
	}
	
	@ManyToOne
	public Customer getCust() {
		return cust;
	}

	public void setCust(Customer cust) {
		this.cust = cust;
	}

	@ManyToOne
	public Offer getOff() {
		return off;
	}

	public void setOff(Offer off) {
		this.off = off;
	}

	@NotNull
	public Boolean getConfirmationSend() {
		return confirmationSend;
	}


	public void setConfirmationSend(Boolean confirmationSend) {
		this.confirmationSend = confirmationSend;
	}

	@Override
	public String toString() {
		return "Contract [id=" + id + ", contractNumber=" + contractNumber + ", entryDate=" + entryDate + ", status="
				+ status + ", comment=" + comment + ", startDate=" + startDate + ", iban=" + iban + ", bic=" + bic
				+ ", selfInstallation=" + selfInstallation + ", installationDate=" + installationDate
				+ ", installedOnDate=" + installedOnDate + ", activatedOnDate=" + activatedOnDate + ", cust=" + cust
				+ ", off=" + off + ", confirmationSend=" + confirmationSend + "]";
	}


	
	
	
}
