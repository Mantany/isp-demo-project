package de.fhwedel.pimpl;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import de.fhwedel.pimpl.data.generator.DataGenerator;
import de.fhwedel.pimpl.data.repo.AddressRepo;
import de.fhwedel.pimpl.data.repo.AvailabilityRepo;
import de.fhwedel.pimpl.data.repo.ContractRepo;
import de.fhwedel.pimpl.data.repo.CustomerRepo;
import de.fhwedel.pimpl.data.repo.OfferRepo;
import de.fhwedel.pimpl.data.service.ContractService;
import de.fhwedel.pimpl.data.service.CustomerService;
import de.fhwedel.pimpl.data.service.OfferService;

@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
public class PIMPLPersistence {

	@Bean
	public Object exampleData(PIMPLConfig config, DataGenerator dg, CustomerService cust_service, ContractService contr_service, OfferService offer_service) {
		if (config.isRegenerate()) {
			dg.generateRandomData(cust_service, contr_service, offer_service);
		}
		return new Object();
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(PIMPLConfig config) {
		Properties props = new Properties();

		if (config.isRegenerate()) {
			props.put(PersistenceUnitProperties.DDL_GENERATION, PersistenceUnitProperties.DROP_AND_CREATE);
			props.put(PersistenceUnitProperties.DDL_GENERATION_MODE, PersistenceUnitProperties.DDL_DATABASE_GENERATION);
		}
		props.put(PersistenceUnitProperties.WEAVING, "" + config.getEclipselink().isWeaving());
		props.put(PersistenceUnitProperties.BATCH_WRITING, config.getEclipselink().getJdbcBatchWriting());
		props.put(PersistenceUnitProperties.TARGET_DATABASE, config.getEclipselink().getTargetDatabase());
		props.put(PersistenceUnitProperties.LOGGING_LEVEL, config.getEclipselink().getLoggingLevel());
		props.put(PersistenceUnitProperties.JDBC_URL, config.getDatasource().getUrl());
		props.put(PersistenceUnitProperties.JDBC_USER, config.getDatasource().getUsername());
		props.put(PersistenceUnitProperties.JDBC_PASSWORD, config.getDatasource().getPassword());
		props.put(PersistenceUnitProperties.JDBC_DRIVER, config.getDatasource().getDriver());

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		EclipseLinkJpaVendorAdapter vendorAdapter = new EclipseLinkJpaVendorAdapter();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPersistenceUnitName(config.getPersistenceunit());
		factory.setJpaProperties(props);
		factory.setPackagesToScan(config.getModelPackage());

		return factory;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}

}
