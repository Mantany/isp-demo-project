#!/bin/sh

export DE_FHWEDEL_PIMPL_DATASOURCE_URL="jdbc:mysql://$APP_DB_HOST:$APP_DB_PORT/pimpl?useUnicode=true&characterEncoding=utf8&useSSL=false"
export DE_FHWEDEL_PIMPL_DATASOURCE_USER=$APP_DB_USER
export DE_FHWEDEL_PIMPL_DATASOURCE_PASSWORD=$APP_DB_PASSWORD
export SPRING_PROFILES_ACTIVE=prod

java -jar /app.jar